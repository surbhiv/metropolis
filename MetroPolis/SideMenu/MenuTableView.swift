//
//  MenuTableView.swift
//  TestSwiftApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import SafariServices

class MenuTableView: UITableViewController,SFSafariViewControllerDelegate {
    
    var menuArray : NSMutableArray = [ "Home","Ask the Expert", "Information Storehouse", "Quiz", "My Questions", "My Notifications", "Settings" , "About Us" , "LOGIN"  ]
    
    var menuImageArray : NSMutableArray = [ "home_sidemenu","ask_sidemenu", "health_sidemenu", "game_sidemenu", "question_sidemenu", "noti_sidemenu", "setting_sidemenu", "aboutus_sidemenu" ,"login_sidemenu"]
    
    
    var selectedMenuItem : Int = 0
    var lastSelectedMenuItem : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ReloadMenuView), name: Constants.pushNotificationToUpdate, object: nil)
        
        
        self.navigationController?.navigationBarHidden = true
        
        // Customize apperance of table view
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0) //
        tableView.separatorStyle = .None
        tableView.backgroundColor = UIColor.clearColor()
        tableView.scrollsToTop = true
        //tableView.bounces = false
       
        let bgImage = UIImageView.init(frame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height))
        bgImage.image = UIImage.init(named: "side_menu_bg")
        tableView.backgroundView = bgImage
        self.view.bringSubviewToFront(tableView)
        
        // Preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    func  ReloadMenuView(){
        
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 140.0
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var headervw = UIView()
        headervw = UIView(frame: CGRectMake(0,0,tableView.frame.size.width,140))
        let image = UIImageView.init(frame: CGRectMake(0,0,tableView.frame.size.width,140))
        image.image = UIImage.init(named: "Open_Menu_Header.jpg")
        headervw.addSubview(image)
        headervw.backgroundColor = UIColor.clearColor()
        self.addSubviewsToMainView(headervw)
        return headervw
    }
    
    func logInFuction()  {
        
        let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LandingView")
        self.presentViewController(destViewController, animated: true, completion: nil);

        
    }
    func logOutFuction()  {
        
        let alertController = UIAlertController.init(title: "Too Shy To Ask", message: "Are you sure, you want to logout?", preferredStyle: UIAlertControllerStyle.Alert)
        let  yesButton = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
            
            let loginManager: FBSDKLoginManager = FBSDKLoginManager()
            loginManager.logOut()
            
            NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.UserID)
            NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.PROFILE)
            NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.PASSWORD)
            NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.showPassword)

            let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LandingView")
            self.presentViewController(destinationController, animated: true, completion: nil);

            
        })
        let noButton = UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Cancel, handler: { (action:UIAlertAction) in
            self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: self.selectedMenuItem, inSection: 0), animated: false, scrollPosition: UITableViewScrollPosition.None)
            
        })
        alertController.addAction(noButton)
        alertController.addAction(yesButton)
        
        self.presentViewController(alertController, animated: true, completion: {
        })

        
    }
    func addSubviewsToMainView(hederView : UIView)
    {
        let profile = UIImageView(frame: CGRectMake(70, 25, 60, 60))
        profile.layer.cornerRadius = 30
        profile.layer.borderWidth = 3
        profile.layer.borderColor = UIColor.init(red: 43.0/255.0, green: 79.0/255.0, blue: 113.0/255.0, alpha: 1).CGColor
        profile.layer.backgroundColor = UIColor.clearColor().CGColor
        profile.image = UIImage.init(named: "profilePlaceHolder")
        profile.contentMode = UIViewContentMode.ScaleAspectFill
        profile.clipsToBounds = true
        
        let nameLabel = UILabel(frame: CGRectMake (10,90,125,45))
        nameLabel.numberOfLines = 0
        nameLabel.backgroundColor = UIColor.clearColor()
        nameLabel.font = UIFont(name: Fonts.extraboldFONTname, size: 14.5)!
        nameLabel.textColor = UIColor.blackColor()
        nameLabel.textAlignment = NSTextAlignment.Right
        
        let editProfileBtn = UIButton(frame: CGRectMake (5,90,190,45))
        editProfileBtn.setImage(UIImage.init(named: "edit_icon"), forState:UIControlState.Normal)
        editProfileBtn.backgroundColor = UIColor.clearColor()
        editProfileBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 145, 0, 10);
        editProfileBtn.titleLabel?.textAlignment = .Center
        
        editProfileBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        editProfileBtn.titleLabel?.font = UIFont(name: Fonts.extraboldFONTname, size: 14.5)!
        editProfileBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        editProfileBtn.addTarget(self, action: #selector(editProfilePressed), forControlEvents: UIControlEvents.TouchUpInside)
        
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
        {
            editProfileBtn.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 15)!
        }
        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil {
            
           nameLabel.text = "Welcome User"
        }
        else
        {
            let dict = NSUserDefaults.standardUserDefaults().objectForKey(Constants.PROFILE) as? NSDictionary
            let tempStr = dict!.valueForKey(Constants.NAME)! as? String
            nameLabel.text = tempStr
            
            let pict = dict!["profile_pic"] as? String
            if pict == "" || pict == nil {
               profile.image = UIImage.init(named: "profilePlaceHolder")
            }
            else
            {
                profile.sd_setImageWithURL(NSURL.init(string: pict!), placeholderImage: UIImage.init(named: "profilePlaceHolder"))
            }
        }
        
        hederView.addSubview(profile)
        hederView.addSubview(nameLabel)
        hederView.addSubview(editProfileBtn)
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 8
        {
            return 90
        }
        return 40;
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return menuArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("CELL")
        
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CELL")
            cell!.backgroundColor = UIColor.clearColor()
            cell!.textLabel?.textColor = UIColor.whiteColor()
            cell!.textLabel?.font = UIFont(name: Fonts.semiboldFONTname, size: 11.5)!
            
            if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
            {
                cell!.textLabel?.font = UIFont(name: Fonts.semiboldFONTname, size: 12.5)!
            }
            let selectedBackgroundView = UIView(frame: CGRectMake(0, 0, cell!.frame.size.width, cell!.frame.size.height - 4.5))
            selectedBackgroundView.backgroundColor = UIColor.clearColor().colorWithAlphaComponent(0.2)
            cell!.selectedBackgroundView = selectedBackgroundView
            
        }
        self.view.bringSubviewToFront(cell!.textLabel!)
        self.view.bringSubviewToFront(cell!.imageView!)

        if indexPath.row == 8
        {
            cell?.selectionStyle = .None
            let loginBtn = UIButton.init(frame: CGRectMake(20, 30, 150, 40))
            loginBtn.titleLabel?.font = UIFont(name: Fonts.boldFONTname, size: 14.5)!
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
            {
                loginBtn.addTarget(self, action: #selector(logInFuction), forControlEvents: .TouchUpInside)
                loginBtn.setTitle("LOGIN", forState: .Normal)
            }
            else
            {
                loginBtn.addTarget(self, action: #selector(logOutFuction), forControlEvents: .TouchUpInside)
                loginBtn.setTitle("LOGOUT", forState: .Normal)
                
            }
            loginBtn.titleLabel?.textColor = UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1.0)
            loginBtn.layer.cornerRadius = 20
            loginBtn.clipsToBounds = true
            loginBtn.backgroundColor = Colors.AppThemeColor
            cell?.contentView.addSubview(loginBtn)

        }
        else
        {
        cell!.textLabel?.text = menuArray[indexPath.row] as? String
        cell!.textLabel?.textColor = UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1.0)
        cell?.imageView?.image = UIImage.init(named: (menuImageArray[indexPath.row] as? String)!)
        cell?.imageView?.backgroundColor = UIColor.clearColor()
        cell?.textLabel?.backgroundColor = UIColor.clearColor()
        
        cell?.backgroundColor = UIColor.clearColor()

        if indexPath.row % 2 == 0 {
            cell?.contentView.backgroundColor = UIColor.init(red: 255.0/250.0, green: 255.0/250.0, blue: 255.0/250.0, alpha: 0.25)

        }
        else
        {
            cell?.contentView.backgroundColor = UIColor.clearColor()
        }

        }
        
        return cell!
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if (indexPath.row == selectedMenuItem && lastSelectedMenuItem != selectedMenuItem){
            
            hideSideMenuView()
            return
        }
        
        //Present new view controller
        var destViewController : UIViewController
        switch (indexPath.row) {
        case 0: //DASHBOARD
            let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("DashBoardVC")as! DashBoardVC
            destViewController.selectedDashBoard = 0
            
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break

        case 1: //AKS THE EXPERT
            let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("DashBoardVC")as! DashBoardVC
            destViewController.selectedDashBoard = 1
            
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 2://Information Storehouse
            destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("HealthTipsVC")
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 3://Quiz
            let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("DashBoardVC")as! DashBoardVC
            destViewController.selectedDashBoard = 2
            
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 4://MY QUESTIONS
            
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
            {
                let alertController = UIAlertController.init(title: "Too Shy To Ask", message: "Please login to continue", preferredStyle: UIAlertControllerStyle.Alert)
                let  yesButton = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    
                    let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LandingView")
                    self.presentViewController(destinationController, animated: true, completion: nil);
                    
                })
                let noButton = UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Cancel, handler: { (action:UIAlertAction) in
                    
                    
                    
                })
                alertController.addAction(noButton)
                alertController.addAction(yesButton)
                
                self.presentViewController(alertController, animated: true, completion: {
                })
                

            }
            else
            {
            
            destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("MyQuestionVC")
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            
            sideMenuController()?.setContentViewController(destViewController)
            }
            break
            
        case 5: //MY NOTIFICATIONS
            let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("DashBoardVC")as! DashBoardVC
            destViewController.selectedDashBoard = 3
            
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 6: //SETTINGS
            
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""  || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
            {
                let alertController = UIAlertController.init(title: "Too Shy To Ask", message: "Please login to continue", preferredStyle: UIAlertControllerStyle.Alert)
                let  yesButton = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    
                    let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LandingView")
                    self.presentViewController(destinationController, animated: true, completion: nil);
                    
                })
                let noButton = UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Cancel, handler: { (action:UIAlertAction) in
                    
                    
                    
                })
                alertController.addAction(noButton)
                alertController.addAction(yesButton)
                
                self.presentViewController(alertController, animated: true, completion: {
                })

            }
            else
            {
                
                destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("EditProfileVC")
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                dispatch_async(dispatch_get_main_queue())
                {
                self.sideMenuController()?.setContentViewController(destViewController)
                }
            }
            
            break
            
        case 7: //About US
            let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("AboutUsVC")as! AboutUsVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
            

        default:
            break
            
        }
    }
    
    func editProfilePressed(){
        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""  || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
        {
            
            let alertController = UIAlertController.init(title: "Too Shy To Ask", message: "Please login to continue", preferredStyle: UIAlertControllerStyle.Alert)
            let  yesButton = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                
                let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LandingView")
                self.presentViewController(destinationController, animated: true, completion: nil);
                
            })
            let noButton = UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Cancel, handler: { (action:UIAlertAction) in
                
                
                
            })
            alertController.addAction(noButton)
            alertController.addAction(yesButton)
            
            self.presentViewController(alertController, animated: true, completion: {
            })

        }
        else
        {
           let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("EditProfileVC")
            lastSelectedMenuItem = selectedMenuItem
            
            sideMenuController()?.setContentViewController(destViewController)
        }
    }
    
    func LoginPressed(){
        if selectedMenuItem != 200 {
            selectedMenuItem = 200
            
            let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView")
            self.presentViewController(destinationController, animated: true, completion: nil);
        }
    }
    
    
    func UPDATEHeaderView()
    {
        dispatch_async(dispatch_get_main_queue())
        {
            let vie = self.tableView(self.tableView, viewForHeaderInSection: 0)
            vie?.setNeedsDisplay()
            self.tableView.reloadSections(NSIndexSet.init(index: 0), withRowAnimation: UITableViewRowAnimation.Automatic)
            self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: self.selectedMenuItem, inSection: 0), animated: false, scrollPosition: .Middle)
        }
    }
    
    func DeleTEModelSelection()
    {
        self .UPDATEHeaderView()
    }
}
