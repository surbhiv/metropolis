//
//  Constant.swift
//  MyRecipeApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import Foundation
import UIKit

struct KEYS {
    static let GoogleKey = "AIzaSyCUSI2vBTEa9MVWXqyCc1aJYVpXNcDV8a0"
    //AIzaSyCUSI2vBTEa9MVWXqyCc1aJYVpXNcDV8a0
}


struct ScreenSize {
    static let SCREEN               = UIScreen.mainScreen().bounds
    static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad
}//&& ScreenSize.SCREEN_MAX_LENGTH >= 1024.0

struct iOSVersion {
    static let SYS_VERSION_FLOAT = (UIDevice.currentDevice().systemVersion as NSString).floatValue
    static let iOS7 = (iOSVersion.SYS_VERSION_FLOAT < 8.0 && iOSVersion.SYS_VERSION_FLOAT >= 7.0)
    static let iOS8 = (iOSVersion.SYS_VERSION_FLOAT >= 8.0 && iOSVersion.SYS_VERSION_FLOAT < 9.0)
    static let iOS9 = (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT < 10.0)
    static let iOS10 = (iOSVersion.SYS_VERSION_FLOAT >= 10.0 && iOSVersion.SYS_VERSION_FLOAT < 11.0)

}


struct Fonts {
    static let regFONTname = "OpenSans"
    static let lightFONTname = "OpenSans-Light"
    static let boldFONTname = "OpenSans-Bold"
    static let semiboldFONTname = "OpenSans-Semibold"
    static let extraboldFONTname = "OpenSans-ExtraBold"
}


struct Constants {
    
    static let pushNotification = "com.neuro.pushNotification"
    static let pushNotificationToUpdate = "sideMenuPushNotification"
    static let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)//(DeviceType.IS_IPAD ? UIStoryboard(name: "Main_iPad",bundle: nil) :)
    
//    static let BASEURL = "http://neuronimbusapps.com/healthcare/api/" // Demo
    static let BASEURL = "http://wellness.metropolisindia.com/healthcare/api/"  // live

    
    static let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    static let PROFILE = "UserProfile"
    static let PASSWORD = "password"
    
    //used for showing change password option
    static let showPassword = "showPassword"

    static let SelectedMenuIndex = "selectedIndex"
    
    static let DevToken = "deviceToken"
    static let AUTHID = "auth_id"
    static let AUTHType = "auth_type"
    static let DEVId = "device_id"
    static let UserID = "user_id"
    static let MOB = "mobile"
    //changes done by jitender
//    static let NAME = "name"
    static let NAME = "first_name"
    static let PROFILEPIC = "profile_pic"
    static let PASS = "password"
    static let STATE = "status"
    static let EMAIL = "email"

    
    static let AuthType = "auth_type"
    static var AuthId = "auth_id"
    static let RUPEE = "₹"
    
    // new added
    static let MESS = "message"

}


struct Colors {
    static let appNavigationColor =  UIColor(red: 58.0/255.0, green: 58.0/255.0, blue: 60.0/255.0, alpha: 1.0)
    static let AppThemeColor = UIColor(red: 11.0/255.0, green:  76.0/255.0, blue: 33.0/255.0, alpha: 1.0)
    static let AppSubThemeColor = UIColor(red: 35.0/255.0, green:  160.0/255.0, blue: 88.0/255.0, alpha: 1.0)
    
    //    static let WindowBackgroudColor = UIColor(red: 78.0/255.0, green:  55.0/255.0, blue: 40.0/255.0, alpha: 1.0)
    static let ShadowColor = UIColor(red: 85.0/255.0, green:  85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
    //    static let Lighttextcolor = UIColor(red: 240.0/255.0, green:  240.0/255.0, blue: 245.0/255.0, alpha: 1.0)

    
}