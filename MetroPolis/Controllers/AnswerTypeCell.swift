//
//  AnswerTypeCell.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 26/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class AnswerTypeCell: UITableViewCell {

    @IBOutlet weak var chatBgImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var replyLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
