//
//  HealthLocationVC.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 17/05/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class HealthLocationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var locationTable: UITableView!
    var categoryName = String()
    var categoryId = String()
    var locationDataArray = [AnyObject]()
    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         titleLbl.text = categoryName
        self.getHealthTipsLocation()
    }
    
    //MARK:- Get category API
    func getHealthTipsLocation() {
        if Reachability.isConnectedToNetwork()
        {
            // NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)
            let method = "/users/healthtips_centres?tips_id=\(categoryId)"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                    self.locationDataArray = response["data"] as! [AnyObject]
                    dispatch_async(dispatch_get_main_queue()){
                        self.locationTable.reloadData()
                    }
                }
                else
                {
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let alert = UIAlertController.init(title: "Too Shy To Ask", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    
                    
                }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    //MARK:- TABLEVIEW
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationDataArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("LocationCell", forIndexPath: indexPath)as! LocationCell
        cell.centerName.text = locationDataArray[indexPath.row]["centre_name"]as? String
        cell.phoneNum.text = locationDataArray[indexPath.row]["phone_no"]as? String
        cell.address.text = locationDataArray[indexPath.row]["address"]as? String

        
        
        tableView.estimatedRowHeight = 145
        tableView.rowHeight = UITableViewAutomaticDimension
        cell.selectionStyle = .None
        return cell  
    }
    
    //MARK:- SIDE MENU BTN
    @IBAction func sideMenuClick(sender: AnyObject) {
        toggleSideMenuView()
        
    }
    
    @IBAction func backBtnClick(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
