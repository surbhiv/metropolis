//
//  QuizVC.swift
//  MetroPolis
//
//  Created by Surbhi on 24/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class QuizVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var thankyouView: UIView!
    @IBOutlet weak var attemptAnswerLbl: UILabel!
    @IBOutlet weak var correctAnswerProgressView: UIProgressView!
    @IBOutlet weak var thankYouPageTable: UITableView!
    @IBOutlet weak var scroller: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var quizcollection: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    var quizUserWrongQuestionArray = [AnyObject]() //storing wrong question data
    //var quizAnswerSendingArray = [AnyObject]()
    var answerTempSendingDict = NSMutableDictionary()
    var jsonStringForAnswer = String()
    var quizId = String()
    var quizArray = [AnyObject]()
    var selectedAnswer = [AnyObject]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        scroller.hidden = false
        thankyouView.hidden = true
        self.getCatogoryOfQuestion()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    //MARK:- Get category API
    func getCatogoryOfQuestion() {
        if Reachability.isConnectedToNetwork()
        {
            // NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)
            let method = "users/quiz/"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                    self.quizArray = response["data"] as! [AnyObject]
                    if self.quizArray.count > 0
                    {
                        if self.quizArray[0]["quiz_id"] != nil
                        {
                            self.quizId = self.quizArray[0]["quiz_id"]as! String
                        }
                    }
                    dispatch_async(dispatch_get_main_queue()){
                        self.quizcollection.reloadData()
                    }
                }
                else
                {
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let alert = UIAlertController.init(title: "Too Shy To Ask", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    
                    
                }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    //MARK: Collection View FlowLayout
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSizeMake(ScreenSize.SCREEN_WIDTH - 15, ScreenSize.SCREEN_HEIGHT - 220)
    }
    
    

    //MARK: Collection View
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return quizArray.count //+ 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("QuizCell", forIndexPath: indexPath) as! QuizCell
        let dict = self.quizArray[indexPath.row]
        let quizArray =  dict["quiz_options"] as! NSArray
        let count = quizArray.count
        
        cell.questionLbl.text = dict["quiz_question"] as? String
        cell.answerLabel1.text = quizArray[0]["optionvalue"] as? String
        cell.answerLabel2.text = quizArray[1]["optionvalue"] as? String
        cell.answerLabel3.text = quizArray[2]["optionvalue"] as? String
        cell.answerLabel4.text = quizArray[3]["optionvalue"] as? String

        cell.answerimagel1.image = UIImage.init(named: "answer_blank")
        cell.answerimagel2.image = UIImage.init(named: "answer_blank")
        cell.answerimagel3.image = UIImage.init(named: "answer_blank")
        cell.answerimagel4.image = UIImage.init(named: "answer_blank")
        
        cell.answerButton1.addTarget(self, action: #selector(Answer1Selected), forControlEvents: UIControlEvents.TouchUpInside)
        cell.answerButton2.addTarget(self, action: #selector(Answer2Selected), forControlEvents: UIControlEvents.TouchUpInside)
        cell.answerButton3.addTarget(self, action: #selector(Answer3Selected), forControlEvents: UIControlEvents.TouchUpInside)
        cell.answerButton4.addTarget(self, action: #selector(Answer4Selected), forControlEvents: UIControlEvents.TouchUpInside)
        
        cell.AnswerView1.backgroundColor = UIColor.init(red: 240.0/255.0, green: 251.0/255.0, blue: 254.0/255.0, alpha: 1)
        cell.AnswerView2.backgroundColor = UIColor.init(red: 240.0/255.0, green: 251.0/255.0, blue: 254.0/255.0, alpha: 1)
        cell.AnswerView3.backgroundColor = UIColor.init(red: 240.0/255.0, green: 251.0/255.0, blue: 254.0/255.0, alpha: 1)
        cell.AnswerView4.backgroundColor = UIColor.init(red: 240.0/255.0, green: 251.0/255.0, blue: 254.0/255.0, alpha: 1)
        
        return cell
    }
    
    func showAlert()
    {
        Helper.openLoginAlertView(self , isShowSkipButton: true)
    }
    
    
    @IBAction func Answer1Selected(sender : AnyObject){
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""  || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
        {
            self.showAlert()
        }
        else
        {
        var indexPath: NSIndexPath!
        var cellSelected : QuizCell!
        
        if let button = sender as? UIButton {
            if let superview = button.superview?.superview {
                if let cell = superview.superview as? QuizCell {
                    indexPath = quizcollection.indexPathForCell(cell)
                    cellSelected = cell
                }
            }
        }
        
        let index = indexPath.item
        let dict = self.quizArray[index]
            
        let quizArray =  dict["quiz_options"] as! NSArray

        cellSelected.AnswerView1.backgroundColor = UIColor.whiteColor()
        cellSelected.answerimagel1.image = UIImage.init(named: "answer_tick")
        
        // work for sending answer array string
        answerTempSendingDict.setValue(quizArray[0]["optionkey"] as! String, forKey: dict["q_id"]as! String)
        
        if dict["answer"] as? String == quizArray[0]["optionkey"] as? String{
        }
        else{
            
            var rightOptionKey = String()
            
            for diction in quizArray{
                let dc = diction as! Dictionary<String,AnyObject>
                if (dc["optionvalue"] as! String) == (dict["answer_value"] as! String){
                    rightOptionKey = (dc["optionkey"] as! String)
                }
            }
            
            
            let sendingDict = ["question" : (dict["quiz_question"]as! String) ,
                               "selectedOption" : (quizArray[0]["optionvalue"] as! String) ,
                               "selectedOptionKey" : (quizArray[0]["optionkey"] as! String),
                               "rightOption" : (dict["answer_value"] as! String),
                               "rightOptionKey" : rightOptionKey
                              ]
            if (!(quizUserWrongQuestionArray as NSArray).containsObject(sendingDict))
            {
            quizUserWrongQuestionArray.append(sendingDict)
            }
        }
        
        self.performSelector(#selector(scrollToNextItem), withObject: NSNumber.init(integer: index), afterDelay: 0.4)
        }
    }
    
    @IBAction func Answer2Selected(sender : AnyObject){
        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""  || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
        {
            self.showAlert()
        }
        else
        {
        
        var indexPath: NSIndexPath!
        var cellSelected : QuizCell!
        
        if let button = sender as? UIButton {
            if let superview = button.superview?.superview{
                if let cell = superview.superview as? QuizCell {
                    indexPath = quizcollection.indexPathForCell(cell)
                    cellSelected = cell
                }
            }
        }
        
        let index = indexPath.item
        let dict = self.quizArray[index]
        let quizArray =  dict["quiz_options"] as! NSArray
        
        cellSelected.AnswerView2.backgroundColor = UIColor.whiteColor()
        cellSelected.answerimagel2.image = UIImage.init(named: "answer_tick")
        
        // work for sending answer array string
        answerTempSendingDict.setValue(quizArray[1]["optionkey"] as! String, forKey: dict["q_id"]as! String)
        
        if dict["answer"] as? String == quizArray[1]["optionkey"] as? String{
        }
        else{
            var rightOptionKey = String()
            
            for diction in quizArray{
                let dc = diction as! Dictionary<String,AnyObject>
                if (dc["optionvalue"] as! String) == (dict["answer_value"] as! String){
                    rightOptionKey = (dc["optionkey"] as! String)
                }
            }
            
            
            let sendingDict = ["question" : (dict["quiz_question"]as! String) ,
                               "selectedOption" : (quizArray[0]["optionvalue"] as! String) ,
                               "selectedOptionKey" : (quizArray[0]["optionkey"] as! String),
                               "rightOption" : (dict["answer_value"] as! String),
                               "rightOptionKey" : rightOptionKey
            ]

            
            if (!(quizUserWrongQuestionArray as NSArray).containsObject(sendingDict))
            {
                quizUserWrongQuestionArray.append(sendingDict)
            }
        }
        self.performSelector(#selector(scrollToNextItem), withObject: NSNumber.init(integer: index), afterDelay: 0.4)
        }
    }
    
    @IBAction func Answer3Selected(sender : AnyObject){
        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""  || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
        {
            self.showAlert()
        }
        else
        {
        
        var indexPath: NSIndexPath!
        var cellSelected : QuizCell!
        
        if let button = sender as? UIButton {
            if let superview = button.superview?.superview {
                if let cell = superview.superview as? QuizCell {
                    indexPath = quizcollection.indexPathForCell(cell)
                    cellSelected = cell
                }
            }
        }
        
        let index = indexPath.item
        let dict = self.quizArray[index]
        let quizArray =  dict["quiz_options"] as! NSArray
        
        cellSelected.AnswerView3.backgroundColor = UIColor.whiteColor()
        cellSelected.answerimagel3.image = UIImage.init(named: "answer_tick")
        
        // work for sending answer array string
        answerTempSendingDict.setValue(quizArray[2]["optionkey"] as! String, forKey: dict["q_id"]as! String)
        
        if dict["answer"] as? String == quizArray[2]["optionkey"] as? String {
        }
        else{
            var rightOptionKey = String()
            
            for diction in quizArray{
                let dc = diction as! Dictionary<String,AnyObject>
                if (dc["optionvalue"] as! String) == (dict["answer_value"] as! String){
                    rightOptionKey = (dc["optionkey"] as! String)
                }
            }
            
            
            let sendingDict = ["question" : (dict["quiz_question"]as! String) ,
                               "selectedOption" : (quizArray[0]["optionvalue"] as! String) ,
                               "selectedOptionKey" : (quizArray[0]["optionkey"] as! String),
                               "rightOption" : (dict["answer_value"] as! String),
                               "rightOptionKey" : rightOptionKey
            ]

            if (!(quizUserWrongQuestionArray as NSArray).containsObject(sendingDict))
            {
                quizUserWrongQuestionArray.append(sendingDict)
            }
        }
        self.performSelector(#selector(scrollToNextItem), withObject: NSNumber.init(integer: index), afterDelay: 0.4)
        }
        
            }
    
    @IBAction func Answer4Selected(sender : AnyObject){
        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""  || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
        {
            self.showAlert()
        }
        else
        {
        
        var indexPath: NSIndexPath!
        var cellSelected : QuizCell!
        
        if let button = sender as? UIButton {
            if let superview = button.superview?.superview {
                if let cell = superview.superview as? QuizCell {
                    indexPath = quizcollection.indexPathForCell(cell)
                    cellSelected = cell
                }
            }
        }
        
        let index = indexPath.item
        let dict = self.quizArray[index]
        let quizArray =  dict["quiz_options"] as! NSArray
        
        cellSelected.AnswerView4.backgroundColor = UIColor.whiteColor()
        cellSelected.answerimagel4.image = UIImage.init(named: "answer_tick")
        
        // work for sending answer array string
        answerTempSendingDict.setValue(quizArray[0]["optionkey"] as! String, forKey: dict["q_id"]as! String)
        
        if dict["answer"] as? String == quizArray[3]["optionkey"] as? String{
        }
        else{
            var rightOptionKey = String()
            
            for diction in quizArray{
                let dc = diction as! Dictionary<String,AnyObject>
                if (dc["optionvalue"] as! String) == (dict["answer_value"] as! String){
                    rightOptionKey = (dc["optionkey"] as! String)
                }
            }
            
            
            let sendingDict = ["question" : (dict["quiz_question"]as! String) ,
                               "selectedOption" : (quizArray[0]["optionvalue"] as! String) ,
                               "selectedOptionKey" : (quizArray[0]["optionkey"] as! String),
                               "rightOption" : (dict["answer_value"] as! String),
                               "rightOptionKey" : rightOptionKey
            ]

            if (!(quizUserWrongQuestionArray as NSArray).containsObject(sendingDict))
            {
                quizUserWrongQuestionArray.append(sendingDict)
            }
        }
        self.performSelector(#selector(scrollToNextItem), withObject: NSNumber.init(integer: index), afterDelay: 0.4)
        }
    }
    
    // MARK: SCROLL TO NEXT ITEM    
    func scrollToNextItem(currtindex : NSNumber){
        
        
        let currentindex = Int(currtindex)
        var newIndex = Int()
        
        if currentindex < quizArray.count - 1 {
            newIndex =  currentindex + 1
            dispatch_async(dispatch_get_main_queue()){
                self.quizcollection.scrollToItemAtIndexPath(NSIndexPath.init(forItem: newIndex, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.Left, animated: false)
            }
        }
        else if currentindex == quizArray.count-1{
            // Thank you Page Shown
             dispatch_async(dispatch_get_main_queue())
             {
                self.jsonStringForAnswer = Helper.convertAnyObjectToJsonString(self.answerTempSendingDict) as String
               // print(self.jsonStringForAnswer)
                if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""  || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
                {
                    self.postAnswerAPI(false)
                }
                else
                {
                    self.postAnswerAPI(true)
                }
                //self.showThankYouPage()
             }
        }
        

    }
    func showThankYouPage()  {
        
        self.thankyouView.hidden = false
        self.thankYouPageTable.reloadData()
        self.view.bringSubviewToFront(self.thankyouView)
        self.attemptAnswerLbl.text = "You Answered \(self.quizArray.count - self.quizUserWrongQuestionArray.count) Out of \(self.quizArray.count) Correctly."
        let correctValue = (self.quizArray.count - self.quizUserWrongQuestionArray.count )
        self.correctAnswerProgressView.progress = Float(correctValue)/Float(self.quizArray.count)
    }
    
    //MARK:- Post method for quiz answer
    
    
    func postAnswerAPI(isHaveUserId : Bool)
    {
        if Reachability.isConnectedToNetwork()
        {
            // NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)
            let method = "users/quiz_reply"
            var param = String()
            if isHaveUserId == true
            {
                param = "user_id=\(NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)!)&quiz_id=\(self.quizId)&answer=\(self.jsonStringForAnswer)"
            }
            else
            {
             param = "user_id=0&quiz_id=\(self.quizId)&answer=\(self.jsonStringForAnswer)"
            }
            
            param = param.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                    dispatch_async(dispatch_get_main_queue())
                    {
                        self.showThankYouPage()
                    }
                }
                else
                {
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let alert = UIAlertController.init(title: "Too Shy To Ask", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    //MARK: - TABLEVIEW FOR THANKYOU PAGE
    //MARK:- DELEGATE
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quizUserWrongQuestionArray.count
    }
    
    //MARK:- DATASOURCE
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("WrongQuestionAttemptCell", forIndexPath: indexPath)as! WrongQuestionAttemptCell
        
        if indexPath.row % 2 == 0
        {
            cell.imageForShadow.hidden = true
        }
        else
        {
            cell.imageForShadow.hidden = false
        }
        
        cell.questionLbl.text = quizUserWrongQuestionArray[indexPath.row]["question"]as? String
        cell.yourAnswerLbl.text = "Your Answer : \((quizUserWrongQuestionArray[indexPath.row]["selectedOptionKey"]as! String).uppercaseString). \(quizUserWrongQuestionArray[indexPath.row]["selectedOption"]as! String)"
        cell.correctAnswerLbl.text = "Right Answer : \((quizUserWrongQuestionArray[indexPath.row]["rightOptionKey"]as! String).uppercaseString). \(quizUserWrongQuestionArray[indexPath.row]["rightOption"]as! String)"

        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
        cell.selectionStyle = .None
        return cell
    }
    @IBAction func goToHomeBtnClicked(sender: AnyObject)
    {
        let destVc = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("DashBoardVC")as! DashBoardVC
        destVc.selectedDashBoard = 0
        self.navigationController?.pushViewController(destVc, animated: true)
        
    }
}
