//
//  MyQuestionVC.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 24/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class MyQuestionVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var myQuestionTable: UITableView!
    @IBOutlet weak var noDataFoundLbl: UILabel!
    var myQuestionDataArray = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noDataFoundLbl.hidden = true
        getMyQuestion()
                // Do any additional setup after loading the view.
    }
    //MARK:- Get My question API
    func getMyQuestion() {
        if Reachability.isConnectedToNetwork()
        {
            //todo
            let method = "users/my_question?user_id=\(NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)!)"
            //let method = "users/my_question?user_id=1"

            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                    
                   self.myQuestionDataArray = response["data"]as! NSArray as [AnyObject]
                    dispatch_async(dispatch_get_main_queue())
                    {
                        if self.myQuestionDataArray.count == 0
                        {
                            self.noDataFoundLbl.hidden = false
                        }
                        else
                        {
                            self.noDataFoundLbl.hidden = true
                        }

                        self.myQuestionTable.reloadData()
                    }
                    
                    
                }
                else
                {
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let alert = UIAlertController.init(title: "Too Shy To Ask", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    
                    
                }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    //MARK:- tableview
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myQuestionDataArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NotificationCell", forIndexPath: indexPath)as! NotificationCell
        
        cell.headingLabel.text = myQuestionDataArray[indexPath.row]["question"] as? String
        cell.descriptionLabel.text = myQuestionDataArray[indexPath.row]["reply"] as? String
        cell.dateLabel.text = Helper.getStringFromDate(myQuestionDataArray[indexPath.row]["created"] as! String)
        cell.readMoreBtn.hidden = false
        cell.readMoreBtn.tag = indexPath.row
        cell.readMoreBtn.addTarget(self, action: #selector(readMoreBtnClicked), forControlEvents: .TouchUpInside)
        tableView.estimatedRowHeight = 130
        tableView.rowHeight = UITableViewAutomaticDimension
        cell.selectionStyle = .None
        return cell
        
    }
    
    func readMoreBtnClicked(sender : UIButton)  {
        let tag = sender.tag
        
        let destVC = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("ChatWithExpertVC")as! ChatWithExpertVC
        destVC.parentQuestionID = String(self.myQuestionDataArray[tag]["question_id"]!!)
        self.navigationController?.pushViewController(destVC, animated: true)
//        let demo1 = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("STBubbleTableViewCellDemoViewController")
//        self.navigationController?.pushViewController(demo1, animated: true)
        
        
    }

    @IBAction func sideMenuClick(sender: AnyObject) {
        toggleSideMenuView()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
