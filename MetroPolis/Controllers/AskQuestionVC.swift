//
//  AskQuestionVC.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 22/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class AskQuestionVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var anonymouseUserBtn: UIButton!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var questionShadowView: UIView!
    @IBOutlet weak var selectCategoryTextfield: UITextField!
    @IBOutlet weak var questionTextview: UITextView!
    
    var categoryArray = [AnyObject]()
    var categoryPicker = UIPickerView()
    var categoryStr = String()
    var categoryIDStr = String()
    var hideUserDetail = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        hideUserDetail = false
        let dic = ["created" : "2017-01-12 00:00:00" , "specialization_id" : "0" , "specialization_name" : "Select Category", "status" : "0" , "updated" : "2017-01-16 00:11:37"]
        categoryArray.append(dic)
        
        self.categoryPicker = UIPickerView.init()
        self.categoryPicker.dataSource = self
        self.categoryPicker.delegate = self
        self.selectCategoryTextfield.inputView = categoryPicker
        

        Helper.giveShadowToView(categoryView)
        Helper.giveShadowToView(questionShadowView)

        self.getCatogoryOfQuestion()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- API
    //MARK:- Get category API
    func getCatogoryOfQuestion() {
        if Reachability.isConnectedToNetwork()
        {
           // NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)
            let method = "users/specialization"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                    
                    let tempArray = response["data"]as! NSArray as [AnyObject]
                    for dict in tempArray
                    {
                        self.categoryArray.append(dict)
                        
                    }


                }
                else
                {
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let alert = UIAlertController.init(title: "Too Shy To Ask", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
  
                    
                }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- Post Question
    
    func postQuestion() {
        if Reachability.isConnectedToNetwork()
        {
            //
            let method = "users/question?"
            var paramString = String()
            if hideUserDetail == true
            {
                paramString = "user_id=\(NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)!)&specialization_id=\(categoryIDStr)&question=\(questionTextview.text)&view_user=1"
            }
            else
            {
                 paramString = "user_id=\(NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)!)&specialization_id=\(categoryIDStr)&question=\(questionTextview.text)&view_user=0"
                
            }
            
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: paramString, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let alert = UIAlertController.init(title: "Too Shy To Ask", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                            self.selectCategoryTextfield.text = ""
                            self.categoryIDStr = ""
                            self.categoryStr = ""
                            self.questionTextview.text = ""
                            
                            let destVC = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("MyQuestionVC")
                            self.navigationController?.pushViewController(destVC, animated: true)
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }

                }
                else
                {
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let alert = UIAlertController.init(title: "Too Shy To Ask", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    
                    
                }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        }
        

    @IBAction func anonymouseUserBtnClicked(sender: AnyObject) {
        if anonymouseUserBtn.selected == true
        {
            anonymouseUserBtn.selected = false
            hideUserDetail = false
        }
        else
        {
            anonymouseUserBtn.selected = true
            hideUserDetail = true
        }
    }

    @IBAction func askDoctorBtnClick(sender: AnyObject) {
        if categoryIDStr == "0" || selectCategoryTextfield.text == "" || categoryIDStr == ""
        {
             CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please select category."])
            
        }
        else if questionTextview.text == "Your Question" || questionTextview.text == ""
        {
             CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please enter your question."])
        }
        else if NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID) == nil
        {
//            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please login to proceed."])
            Helper.openLoginAlertView(self , isShowSkipButton: false)
            
        }
        else
        {
            postQuestion()
        }
    }
    
    
    //MARK:- picker delegate
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return categoryArray.count
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let value = categoryArray [row]["specialization_name"] as! String
        return String.init(format: value , locale: nil)
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        //        let value = tagTypeArray [row]["TagId"] as! Int
        let value = categoryArray [row]["specialization_id"]as! String
        categoryIDStr = value
        
        let value1 = categoryArray [row]["specialization_name"] as! String
        
        self.selectCategoryTextfield.text = value1
    }
    
    // MARK: TEXTFIELD DELEGATE
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        textField .resignFirstResponder()
        //self.Searchtext = self.searchTextfield.text!
        
    }

    // MARK: TEXTVIEW DELEGATE
    
    func textViewDidBeginEditing(textView: UITextView) {
        questionTextview.text = ""
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        
        var val = questionTextview.text
        val = val.stringByReplacingOccurrencesOfString(" ", withString: "")
        if  val == "" {
            questionTextview.text = "Your Question"
        }
    }
}
