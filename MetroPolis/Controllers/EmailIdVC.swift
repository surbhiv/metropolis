//
//  EmailIdVC.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 02/05/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit
protocol sendEmailId {
    func getEmailIdStr(emailStr : String)
}
class EmailIdVC: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!

    var sendEmailDelegate : sendEmailId?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func cancelBtnClick(sender: AnyObject)
    {
        emailTextField.resignFirstResponder()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func OkBtnClick(sender: AnyObject)
    {
        if emailTextField.text == "" {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Too Shy To Ask", "message": "Please enter a email id."] )
        }
        else if !Helper.isValidEmail(emailTextField.text!) {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Too Shy To Ask", "message": "Enter a valid email id."] )
        }
        else
        {
            sendEmailDelegate?.getEmailIdStr(emailTextField.text!)
            emailTextField.resignFirstResponder()
            self.dismissViewControllerAnimated(true, completion: nil)
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
