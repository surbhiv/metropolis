//
//  HealthTipsVC.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 17/05/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class HealthTipsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

   
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var healthTipsTable: UITableView!
    var dataArray = [AnyObject]()
    var isNotFromSideMenu = Bool()
    
    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      
        if isNotFromSideMenu == true
        {
            backBtn.hidden = false
        }
        Helper.giveShadowToView(shadowView)
        self.getCatogoryOfHealthTips()
        
//        let dict1 = ["heading" : "Puberty and reproduction", "subHeading" : "Sed ut perspiciatis dfdf dsvfdfv vdsv","image" : "Puberty_icon"]
//        let dict2 = ["heading" : "Sexual & reproduction Health", "subHeading" : "Sed ut perspiciatis dfdf dsvfdfv vdsv", "image" : "Sexual_&_Reproduction_Health_icon"]
//        let dict3 = ["heading" : "Sexual Violence", "subHeading" : "Sed ut perspiciatis dfdf dsvfdfv vdsv", "image" : "Sexual_Violence_icon"]
//        let dict4 = ["heading" : "Substance Abuse", "subHeading" : "Sed ut perspiciatis dfdf dsvfdfv vdsv", "image" : "Substance_Abuse_icon"]
//        let dict5 = ["heading" : "Gender Equity", "subHeading" : "Sed ut perspiciatis dfdf dsvfdfv vdsv", "image" : "Gender_Equity_icon"]
//        let dict6 = ["heading" : "Nutrition, Exercise & Vaccines", "subHeading" : "Sed ut perspiciatis dfdf dsvfdfv vdsv", "image" : "Nutrition_Exercises_&_Vaccines_icon"]
//        let dict7 = ["heading" : "Myth Busters", "subHeading" : "Sed ut perspiciatis dfdf dsvfdfv vdsv", "image" : "Myth_Busters_icon"]
//        
//        dataArray.append(dict1)
//        dataArray.append(dict2)
//        dataArray.append(dict3)
//        dataArray.append(dict4)
//        dataArray.append(dict5)
//        dataArray.append(dict6)
//        dataArray.append(dict7)

    }
    
    //MARK:- Get category API
    func getCatogoryOfHealthTips() {
        if Reachability.isConnectedToNetwork()
        {
            // NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)
            let method = "users/healthtips"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                    self.dataArray = response["data"] as! [AnyObject]
                    dispatch_async(dispatch_get_main_queue()){
                        self.healthTipsTable.reloadData()
                    }
                }
                else
                {
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let alert = UIAlertController.init(title: "Too Shy To Ask", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    
                    
                }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- TABLEVIEW
    //TABLEVIEW DELEGATE
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    //TABLEVIEW DATASOURSE
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("homeHeaderTableViewCell", forIndexPath: indexPath)
        let headingImage = cell.contentView.viewWithTag(1001)as! UIImageView
        let headingLbl = cell.contentView.viewWithTag(1002)as! UILabel
//        let subHeadingLbl = cell.contentView.viewWithTag(1003)as! UILabel
        headingLbl.text = dataArray[indexPath.row]["tips_title"]as? String
//        subHeadingLbl.text = dataArray[indexPath.row]["tips_short_description"]as? String
        headingImage.sd_setImageWithURL(NSURL.init(string: dataArray[indexPath.row]["tips_image"]as! String))
        // headingImage.image = UIImage.init(named: dataArray[indexPath.row]["tips_image"]as! String)
        
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let destVC = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("HealthTipsDetailVC")as! HealthTipsDetailVC
        destVC.categoryId = dataArray[indexPath.row]["tips_id"]as! String
        destVC.categoryName = dataArray[indexPath.row]["tips_title"]as! String
        destVC.categoryDetailURL = dataArray[indexPath.row]["tips_descriptions"]as! String
        let showCenterValue = dataArray[indexPath.row]["is_show_centre"]as! String
        if showCenterValue == "1"
        {
            destVC.isShowCenterBtn = true
        }
        else
        {
            destVC.isShowCenterBtn = false
        }
        self.navigationController?.pushViewController(destVC, animated: true)
    }

    //MARK:- SIDE MENU BTN
    @IBAction func sideMenuClick(sender: AnyObject) {
        toggleSideMenuView()
        
    }
    @IBAction func backBtnClick(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
