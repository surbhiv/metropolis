//
//  DashBoardVC.swift
//  MetroPolis
//
//  Created by Surbhi on 20/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class DashBoardVC: UIViewController, ENSideMenuDelegate , UIScrollViewDelegate {
    @IBOutlet weak var titleHomeImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!

    private var indicatorLabel : UILabel!
  
    private var ControllerArray = [AnyObject]()
    private var headerArray = ["Home","Ask","Gaming","Notice"]
    private var headerImageArray = ["HomeIcon","AskIcon","GamingIcon","NoticeIcon"]

    private var productArray = [AnyObject]()
    
    var selectedDashBoard : Int = 0
    var homeSubView = HomeVC()
    var askQuestionSubView = AskQuestionVC()
    var notificationSubView = NotificationVC()
    var playQuizSubView = QuizVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sideMenuController()?.sideMenu?.delegate = self
        self.navigationController?.navigationBarHidden = true
        titleLabel.hidden = true
        titleHomeImage.hidden = false
        self.setHeaderScroll()
        self.setUpScrollView()
        let btn : UIButton
        btn =  UIButton.init()
        btn.tag = selectedDashBoard
        self.HeaderlabelSelected(btn)
        // Do any additional setup after loading the view.
        if NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID) == nil
        {
         
            Helper.openLoginAlertView(self ,isShowSkipButton: false)

            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- SIDE MENU ACTIONS
    @IBAction func toggleSideMenu(sender: AnyObject) {
        askQuestionSubView.questionTextview.resignFirstResponder()
        askQuestionSubView.selectCategoryTextfield.resignFirstResponder()
        toggleSideMenuView()
    }

    // MARK: - Label ScrollView
    
    func setHeaderScroll() {
        var scrollContentCount = 0;
        var frame : CGRect = CGRectMake(0, 0, 0, 0)
        var labelx : CGFloat = 0.0
        let width  : CGFloat = ScreenSize.SCREEN_WIDTH / CGFloat(headerArray.count)
        let firstLabelWidth : CGFloat = ScreenSize.SCREEN_WIDTH / CGFloat(headerArray.count)
        
        indicatorLabel = UILabel.init(frame: CGRectMake(labelx, 38, firstLabelWidth, 3))
        indicatorLabel.backgroundColor = UIColor.whiteColor()
        indicatorLabel.text = "";
        headerScroll.addSubview(indicatorLabel)
        
        for arrayIndex in 0 ..< headerArray.count {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0;
            frame.size.width=width;
            frame.size.height=39;
            
            let accountLabelButton = UIButton.init(type: UIButtonType.Custom)
            accountLabelButton.addTarget(self, action:#selector(HeaderlabelSelected), forControlEvents: UIControlEvents.TouchUpInside)
            
            accountLabelButton.frame = frame;
            accountLabelButton.tag = arrayIndex;
            
            accountLabelButton.setImage(UIImage.init(named: headerImageArray[arrayIndex]), forState: UIControlState.Normal)
                        
            headerScroll.addSubview(accountLabelButton)
            
            if arrayIndex == 0 {
                accountLabelButton.alpha=1.0;
            }
            else {
                accountLabelButton.alpha=0.7;
            }
            
            labelx=labelx+width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        // set scroll content size
        
        dispatch_async(dispatch_get_main_queue(),{
            self.headerScroll.contentSize = CGSizeMake(frame.width, 40)
            self.detailScroll.scrollRectToVisible(CGRectMake(ScreenSize.SCREEN_WIDTH * CGFloat(self.selectedDashBoard), 0, ScreenSize.SCREEN_WIDTH, self.detailScroll.frame.size.height), animated: false)
        })
        automaticallyAdjustsScrollViewInsets = false;
        detailScroll.delegate=self;
    }
    
    
    func HeaderlabelSelected(sender: UIButton) {
        detailScroll.tag = sender.tag
        
        let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag)
        detailScroll.setContentOffset(CGPointMake(xAxis, 0), animated: true)
        
        let buttonArray = NSMutableArray()
        selectedDashBoard = sender.tag
        for view in headerScroll.subviews {
            if view.isKindOfClass(UIButton) {
                let labelButton = view as! UIButton
                buttonArray.addObject(labelButton)
                
                if labelButton.tag == sender.tag {
                    labelButton.alpha = 1.0
                }
                else {
                    labelButton.alpha = 0.7
                }
            }
        }
        
        let labelButton = buttonArray.objectAtIndex(sender.tag)
        var frame : CGRect = labelButton.frame
        frame.origin.y = 38
        frame.size.height = 3
        if sender.tag == 0
        {
            self.titleLabel.hidden = true
            self.titleHomeImage.hidden = false
        }
        else if sender.tag == 1
        {
            dispatch_async(dispatch_get_main_queue(),{
                self.titleLabel.hidden = false
                self.titleHomeImage.hidden = true
                self.titleLabel.text = "ASK THE EXPERTS"
            })
        }
        else if sender.tag == 2
        {
            dispatch_async(dispatch_get_main_queue(),{
            self.titleLabel.hidden = false
            self.titleHomeImage.hidden = true
            self.titleLabel.text = "QUIZ"
                })
            
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""  || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
            {
                
                Helper.openLoginAlertView(self , isShowSkipButton: true)
            }
        }
        else if sender.tag == 3
        {
            dispatch_async(dispatch_get_main_queue(),{
                self.titleLabel.hidden = false
                self.titleHomeImage.hidden = true
                self.titleLabel.text = "NOTIFICATION"
            })
        }

        UIView.animateWithDuration(0.2, animations: {
            if sender.tag == 0 {
                self.indicatorLabel.frame =  CGRectMake(0,38, ScreenSize.SCREEN_WIDTH/CGFloat(self.headerArray.count), 3)
            }
            else {
                self.indicatorLabel.frame =  CGRectMake((ScreenSize.SCREEN_WIDTH/CGFloat(self.headerArray.count)) * CGFloat(sender.tag) ,38, ScreenSize.SCREEN_WIDTH / CGFloat(self.headerArray.count), 3)
            }
            
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)
            
            }, completion: nil)
    }
    
    // MARK: -  Main Scrollview
    func setUpScrollView() {
        for arrayIndex in headerArray {
            if (arrayIndex == "Home")  {
                homeSubView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("HomeVC") as! HomeVC
                
                ControllerArray.append(homeSubView)
            }
            else if (arrayIndex == "Ask")
            {
                askQuestionSubView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("AskQuestionVC") as! AskQuestionVC
                
                ControllerArray.append(askQuestionSubView)

            }
            else if (arrayIndex == "Gaming")
            {
                playQuizSubView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("QuizVC") as! QuizVC
//                
                ControllerArray.append(playQuizSubView)
            }
            else if (arrayIndex == "Notice")
            {
                notificationSubView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("NotificationVC") as! NotificationVC
                
                ControllerArray.append(notificationSubView)
                
            }

            
        }
        setViewControllers(ControllerArray, animated: false)
    }
    
    
    //  Add and remove view controllers
    func setViewControllers(viewControllers : NSArray, animated : Bool) {
        if self.childViewControllers.count > 0 {
            for vC in self.childViewControllers {
                vC.willMoveToParentViewController(nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers {
            self.addChildViewController(vC as! UIViewController)
            vC.didMoveToParentViewController(self)
        }
        
        //TODO animations
        if ((detailScroll) != nil) {
            reloadPages()
        }
    }
    
    func reloadPages() {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count {
            // set scorllview properties
            var frame : CGRect = CGRectMake(0, 0, 0, 0)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = ScreenSize.SCREEN.size
            
            detailScroll.pagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            widthConstraint.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            addView(vC.view, contentView: contentView, frame: frame)
            
            scrollContentCount = scrollContentCount + 1;
        }
        
        dispatch_async(dispatch_get_main_queue(),{
            self.detailScroll.contentSize = CGSizeMake(self.widthConstraint.constant, self.detailScroll.frame.size.height)
        })
        detailScroll.delegate = self
    }
    
    // adding views to scrollview
    func addView(view : UIView, contentView : UIView, frame : CGRect) {
        view.frame = frame
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        dispatch_async(dispatch_get_main_queue(),{
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute:  NSLayoutAttribute.Leading, multiplier: 1.0, constant: frame.origin.x))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute:  NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: frame.size.width))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute:  NSLayoutAttribute.Top, multiplier: 1.0, constant:0))
            
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute:  NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: frame.size.height - 100))
        })
    }

}
