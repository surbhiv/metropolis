//
//  EditProfileVC.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 28/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate  {

    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var newEditProfileBtn: UIButton!
    @IBOutlet weak var fnameLbl: UILabel!
    @IBOutlet weak var fnameText: UITextField!
    @IBOutlet weak var fnameLineLbl: UILabel!
    
    @IBOutlet weak var lnameLbl: UILabel!
    @IBOutlet weak var lnameText: UITextField!
    @IBOutlet weak var lnameLineLbl: UILabel!
    
    @IBOutlet weak var mobileLbl: UILabel!
    @IBOutlet weak var mobileText: UITextField!
    @IBOutlet weak var mobileLineLbl: UILabel!
    
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var emailLineLbl: UILabel!
    
    
    
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var changePasswordBtn: UIButton!
    @IBOutlet weak var deactivateBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    var imageDataIMG = NSData()
    var imageDataToSend = String()
    var imagePicker:UIImagePickerController?=UIImagePickerController()
    var isHaveImage = Bool()
    var profileDataDict = Dictionary<String, AnyObject>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(logOutCalled), name: "PasswordChanged", object: nil)
        
        
        uploadBtn.userInteractionEnabled = false
        self.saveBtn.hidden = true
        userImage.layer.cornerRadius = 45
        userImage.layer.masksToBounds = true
        isHaveImage = false
        imagePicker?.delegate = self
        emailText.userInteractionEnabled = false
        editBtn.hidden = false
        self.disableAllField()
        self.getProfileData()
        
        // Do any additional setup after loading the view.
    }
    func checkForPassword ()
    {
      
        //let passwordBlank = String(NSUserDefaults.standardUserDefaults().objectForKey(Constants.PASSWORD)!)
        
        let newCheckPasswordStr =  String(NSUserDefaults.standardUserDefaults().objectForKey(Constants.showPassword)!)
       
        //passwordBlank == "0" ||
        if  newCheckPasswordStr == "HavePassword"
        {
            changePasswordBtn.hidden = false
            newEditProfileBtn.hidden = true
            
        }
        else
        {
            changePasswordBtn.hidden = true
            editBtn.hidden = true
            newEditProfileBtn.hidden = false
        }

    }
    func getProfileData()
    {
        profileDataDict = NSUserDefaults.standardUserDefaults().objectForKey(Constants.PROFILE)! as! Dictionary<String, AnyObject>

        self.checkForPassword()
        fnameText.text = profileDataDict["first_name"]as? String
        lnameText.text = profileDataDict["last_name"]as? String
        mobileText.text = profileDataDict["phone"]as? String
        emailText.text = profileDataDict["email"]as? String
        if profileDataDict["profile_pic"] != nil
        {
            self.userImage.sd_setImageWithURL(NSURL.init(string: profileDataDict["profile_pic"]as! String), placeholderImage: UIImage.init(named: "profilePlaceHolder"))
            self.userImage.setShowActivityIndicatorView(true)
            self.userImage.setIndicatorStyle(.Gray)

            
        }
        
    }
    func disableAllField()  {
        fnameText.userInteractionEnabled = false
        lnameText.userInteractionEnabled = false
        mobileText.userInteractionEnabled = false
        emailText.userInteractionEnabled = false

    }
    
    func activeAllField()  {
        fnameText.userInteractionEnabled = true
        lnameText.userInteractionEnabled = true
        mobileText.userInteractionEnabled = true
//        emailText.userInteractionEnabled = false
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileText {
            if (range.location > 9)
            {
                return false
            }
        }
        
        return true
    }

    @IBAction func sideMenuClicked(sender: AnyObject) {
        toggleSideMenuView()
    }
    @IBAction func saveBtnClicked(sender: AnyObject)
    {
        if self.fnameText.text == ""
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Too Shy To Ask", "message": "Please enter first name"] )
        }
        else if self.lnameText.text == ""
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Too Shy To Ask", "message": "Please enter last name"] )
        }
        else if self.mobileText.text == ""
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Too Shy To Ask", "message": "Please enter mobile number"] )
        }
        else if self.mobileText.text?.characters.count < 10
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Too Shy To Ask", "message": "Please enter valid phone number"] )
        }
        else
        {
            self.updateUserProfile()
        }

        
    }
    @IBAction func changePasswordBtnClicked(sender: AnyObject)
    {
//        let destVC = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("ForgotPasswordView")as! ForgotPasswordView
//        
//        let topview = self.parentViewController
//        //        topview.addSubview(destVC.view)
//        topview!.addChildViewController(destVC)
//        destVC.didMoveToParentViewController(topview)
//        topview?.view.addSubview(destVC.view)
        
        let forgot = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("ForgotPassword") as! ForgotPasswordView
        forgot.isForgetView = false
        forgot.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        self.presentViewController(forgot, animated: true, completion: nil)

        
    }
    
    @IBAction func deactivateBtnClicked(sender: AnyObject) {
    }
    @IBAction func editBtnClicked(sender: AnyObject) {
        self.activeAllField()
        editBtn.hidden = true
        uploadBtn.userInteractionEnabled = true

        self.saveBtn.hidden = false
    }
    
    @IBAction func uploadPhotoBtnClick(sender: AnyObject)
    {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .ActionSheet)
        
        // 2
        let cameraAction = UIAlertAction(title: "Open Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
            
        })
        let galleryAction = UIAlertAction(title: "Open Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallary()
            
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    
    
    
    //MARK:- IMAGEPICKERDELEGATE
    func openGallary()
    {
        imagePicker!.allowsEditing = true
        imagePicker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        presentViewController(imagePicker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            imagePicker!.allowsEditing = true
            imagePicker!.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker!.cameraCaptureMode = .Photo
            presentViewController(imagePicker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .Alert)
            let ok = UIAlertAction(title: "OK", style:.Default, handler: nil)
            alert.addAction(ok)
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            //self.userImage.contentMode = .ScaleAspectFit
            
            // let resizedImage = Helper.resizeImage(pickedImage, newWidth: 400.0)
            imageDataIMG = UIImagePNGRepresentation(pickedImage)! as NSData
            
            let imageSize: Int = imageDataIMG.length
            let size = imageSize/1024

            if size > 1024
            {
                isHaveImage = false
                dispatch_async(dispatch_get_main_queue())
                {
                    self.performSelector(#selector(self.imageAlert), withObject: nil, afterDelay: 0.5)
                }
                dismissViewControllerAnimated(true, completion: nil)
                
                return
            }
            else
            {
                isHaveImage = true
                self.userImage.image = pickedImage
                
                dismissViewControllerAnimated(true, completion: nil)
                
                
            }
            
            //            self.updateUserProfileImage()
        }
        
        
        
    }
    func imageAlert()
    {
        let alert = UIAlertController.init(title: "Too Shy To Ask", message: "Please select image of size less than 1 MB", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
        
    }
    
    //MARK:- UPDATE PROFILE
    func updateUserProfile() {
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            if isHaveImage == true
            {
                imageDataToSend = imageDataIMG.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
            }
            else
            {
                imageDataToSend = ""
            }
            
            let method = "users/edit_profile"
            
            var param = "user_id=\(NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)!)&first_name=\(fnameText.text!)&last_name=\(lnameText.text!)&phone=\(mobileText.text!)&profile_pic=\(imageDataToSend)"
            param = param.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param, completionHandler: { (response) in
            
                Constants.appDelegate.stopIndicator()
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                    dispatch_async(dispatch_get_main_queue())
                    {
                        self.saveBtn.hidden = true
                        self.editBtn.hidden = false
                        self.uploadBtn.userInteractionEnabled = false

                        self.checkForPassword()
                        self.disableAllField()
                        if self.isHaveImage == true
                        {
                           self.isHaveImage = false
                        }
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"User profile updated successfully."])
                        self.profileDataDict["first_name"] = response["data"]!["first_name"]as! String
                        self.profileDataDict["last_name"] = response["data"]!["last_name"]as! String
                        self.profileDataDict["phone"] = response["data"]!["phone"]as! String
                        self.profileDataDict["profile_pic"] = response["data"]!["profile_pic"]as! String
                        
                        NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.PROFILE)
                    NSUserDefaults.standardUserDefaults().setObject(self.profileDataDict, forKey: Constants.PROFILE)
                    NSUserDefaults.standardUserDefaults().synchronize()
                        
                    //self.getProfileData()
                        let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                        mo.UPDATEHeaderView()
                        
                    }
                   
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let mess = response["message"] as! String
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":mess])
                    }
                }
                }
            })
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    //MARK:- LogOut(Change password)
    func logOutCalled()  {
        Helper.logOutFunction(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
