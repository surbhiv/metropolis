//
//  QuizCell.swift
//  MetroPolis
//
//  Created by Surbhi on 24/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class QuizCell: UICollectionViewCell {
    
    @IBOutlet weak var QuestionView: UIView!
    @IBOutlet weak var questionLbl: UILabel!

    
    @IBOutlet weak var AnswerView1: UIView!
    @IBOutlet weak var answerLabel1: UILabel!
    @IBOutlet weak var answerimagel1: UIImageView!
    @IBOutlet weak var answerButton1: UIButton!

    
    @IBOutlet weak var AnswerView2: UIView!
    @IBOutlet weak var answerLabel2: UILabel!
    @IBOutlet weak var answerimagel2: UIImageView!
    @IBOutlet weak var answerButton2: UIButton!

    
    @IBOutlet weak var AnswerView3: UIView!
    @IBOutlet weak var answerLabel3: UILabel!
    @IBOutlet weak var answerimagel3: UIImageView!
    @IBOutlet weak var answerButton3: UIButton!

    
    @IBOutlet weak var AnswerView4: UIView!
    @IBOutlet weak var answerLabel4: UILabel!
    @IBOutlet weak var answerimagel4: UIImageView!
    @IBOutlet weak var answerButton4: UIButton!

    @IBOutlet weak var AnswerView5: UIView!
    @IBOutlet weak var answerLabel5: UILabel!
    @IBOutlet weak var answerimagel5: UIImageView!
    @IBOutlet weak var answerButton5: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        Helper.giveShadowToView(QuestionView)
        Helper.giveShadowToView(AnswerView1)
        Helper.giveShadowToView(AnswerView2)
        Helper.giveShadowToView(AnswerView3)
        Helper.giveShadowToView(AnswerView4)
//        Helper.giveShadowToView(AnswerView5)



        
    }
    

    
}
