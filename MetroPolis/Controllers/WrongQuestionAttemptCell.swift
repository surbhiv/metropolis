//
//  WrongQuestionAttemptCell.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 28/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class WrongQuestionAttemptCell: UITableViewCell {

    @IBOutlet weak var imageForShadow: UIImageView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var yourAnswerLbl: UILabel!
    @IBOutlet weak var correctAnswerLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //Helper.giveShadowToView(self.contentView)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
