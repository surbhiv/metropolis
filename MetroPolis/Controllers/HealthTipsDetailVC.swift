//
//  HealthTipsDetailVC.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 17/05/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class HealthTipsDetailVC: UIViewController,UIWebViewDelegate, UIScrollViewDelegate {

    @IBOutlet weak var heightConstantForBtn: NSLayoutConstraint!
    @IBOutlet weak var rehabeBtn: UIButton!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var noDataFoundLbl: UILabel!
    @IBOutlet weak var healthDetailWebView: UIWebView!
    var categoryName = String()
    var categoryId = String()
    var categoryDetailURL = String()
    var isShowCenterBtn = Bool()
    var isAnotherLinkOpen = Bool()
    var mainURL = String()
    var selectedURL = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.healthDetailWebView.scrollView.delegate = self
        
        // Do any additional setup after loading the view.
        if isShowCenterBtn == true
        {
            rehabeBtn.hidden = false
            heightConstantForBtn.constant = 50
        }
        else
        {
            rehabeBtn.hidden = true
            heightConstantForBtn.constant = 0
        }
       
        let tempStr = "rehab centers for the \(categoryName)"
        rehabeBtn.setTitle(tempStr.uppercaseString, forState: .Normal)
        rehabeBtn.titleLabel?.textAlignment = .Center
        
        healthDetailWebView.delegate = self
        
        titleLbl.text = categoryName
        if categoryDetailURL == ""  {
            noDataFoundLbl.hidden = false
        }
        else{
            noDataFoundLbl.hidden = true
            
            self.reloadWebview()

        }
       
    }
    
    func reloadWebview(){
        let urlRequest = NSMutableURLRequest(URL: NSURL(string: categoryDetailURL)!)
        urlRequest.HTTPMethod = "GET"
        
        // Handling Basic HTTPS Authorization
        let authString = "healthcare@gmail.com:Neuro@123"
        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)
        let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        mainURL = String(urlRequest.URL!)
        selectedURL = mainURL
        healthDetailWebView.loadRequest(urlRequest)
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        Constants.appDelegate.startIndicator()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        
        Constants.appDelegate.stopIndicator()
        
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        Constants.appDelegate.stopIndicator()
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {

        switch (navigationType) {
        case UIWebViewNavigationType.LinkClicked:
            // user clicked on link
            selectedURL = String(request.URL!)
            break;
        case UIWebViewNavigationType.Other:
            // request was caused by an image that's being loaded
            break;
            
        default:
            break
        }

        if selectedURL == mainURL {
            isAnotherLinkOpen = false
        }
        else{
            isAnotherLinkOpen = true
        }

        
        return true

    }
    
   
    
    

    @IBAction func viewRehabilitationBtnClick(sender: AnyObject) {
        let destVC = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("HealthLocationVC")as! HealthLocationVC
        destVC.categoryName = self.categoryName
        destVC.categoryId = self.categoryId
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    //MARK:- SIDE MENU BTN
    @IBAction func sideMenuClick(sender: AnyObject) {
        toggleSideMenuView()
        
    }
    @IBAction func backBtnClick(sender: AnyObject) {
        
        if isAnotherLinkOpen == false{
            self.navigationController?.popViewControllerAnimated(true)
        }
        else{
            self.reloadWebview()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
