//
//  LocationCell.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 07/06/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {

    @IBOutlet weak var centerName: UILabel!
    @IBOutlet weak var phoneNum: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var shadowView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Helper.giveShadowToView(shadowView)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
