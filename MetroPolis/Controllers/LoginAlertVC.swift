//
//  LoginAlertVC.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 06/06/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class LoginAlertVC: UIViewController {

    @IBOutlet weak var skipBtn: UIButton!
    var isShowSkipBtn = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isShowSkipBtn == true
        {
            skipBtn.hidden = false
        }
        else
        {
            skipBtn.hidden = true
        }
    }

    @IBAction func tapGestureClick(sender: AnyObject) {
        
        print("tap clicked")
        if isShowSkipBtn == false
        {
        self.dismissViewControllerAnimated(false) {
            
        }
        }
    }
    @IBAction func skipToHomeClicked(sender: AnyObject) {
        
        if Constants.appDelegate.window?.rootViewController?.childViewControllers.last?.isKindOfClass(DashBoardVC) == true
        {
           let obj = Constants.appDelegate.window?.rootViewController?.childViewControllers.last as! DashBoardVC
            let btn : UIButton
            btn = UIButton.init()
            btn.tag = 0
            obj.HeaderlabelSelected(btn)
            self.dismissViewControllerAnimated(false) {
                
            }

        }
       
       
    }
    @IBAction func loginBtnClick(sender: AnyObject) {
        let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LandingView")
        self.presentViewController(destViewController, animated: true, completion: nil);
    }
    @IBAction func registerBtnClick(sender: AnyObject)
    {
        let login = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RegisterationView") as! RegisterationView
        self.presentViewController(login, animated: true, completion: nil)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
