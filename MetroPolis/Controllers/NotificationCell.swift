//
//  NotificationCell.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 24/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var readMoreBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Helper.giveShadowToView(shadowView)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
