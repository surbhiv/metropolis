//
//  NotificationVC.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 24/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var notificationTable: UITableView!
    @IBOutlet weak var noDataFoundLbl: UILabel!
    var notificationDataArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        if NSUserDefaults.standardUserDefaults().objectForKey("PUSH_MESSAGES") != nil
        {
        notificationDataArray = NSUserDefaults.standardUserDefaults().objectForKey("PUSH_MESSAGES")?.mutableCopy() as! NSMutableArray
             noDataFoundLbl.hidden = true
        }
        self.checkForNoData()
        notificationTable.reloadData()
        // Do any additional setup after loading the view.
    }
    
    func reloadCell()  {
        if NSUserDefaults.standardUserDefaults().objectForKey("PUSH_MESSAGES") != nil
        {
            notificationDataArray = NSUserDefaults.standardUserDefaults().objectForKey("PUSH_MESSAGES")?.mutableCopy() as! NSMutableArray
        }
        self.checkForNoData()
        notificationTable.reloadData()
    }
    func checkForNoData()  {
        
       dispatch_async(dispatch_get_main_queue())
       {
        if self.notificationDataArray.count == 0
        {
            self.noDataFoundLbl.hidden = false
        }
        else
        {
            self.noDataFoundLbl.hidden = true
        }
        }
    }

    //MARK:- tableview
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("Notification Array Count - \(notificationDataArray.count)")
        return notificationDataArray.count//notificationDataArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NotificationCell", forIndexPath: indexPath)as! NotificationCell
        cell.headingLabel.text = notificationDataArray[indexPath.row]["alert"]as? String
        cell.descriptionLabel.text = notificationDataArray[indexPath.row]["description"]as? String
        cell.dateLabel.text = Helper.getStringFromDate(notificationDataArray[indexPath.row]["date"]as! String)
        tableView.estimatedRowHeight = 128
        tableView.rowHeight = UITableViewAutomaticDimension
        cell.selectionStyle = .None
        return cell
        
    }
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete
        {
            notificationDataArray.removeObjectAtIndex(indexPath.row)

            if NSUserDefaults.standardUserDefaults().objectForKey("PUSH_MESSAGES") != nil
            {
            NSUserDefaults.standardUserDefaults().removeObjectForKey("PUSH_MESSAGES")
                NSUserDefaults.standardUserDefaults().setObject(notificationDataArray, forKey: "PUSH_MESSAGES")
            }
            self.checkForNoData()
            tableView.deleteRowsAtIndexPaths([NSIndexPath.init(forRow: indexPath.row, inSection: 0)], withRowAnimation: .None)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
