//
//  LoginView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 26/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
//import Firebase
//import Google
//import FacebookCore
//import FacebookLogin


class LandingView: UIViewController,sendEmailId {

    // MARK: IBOutlets Defined
    @IBOutlet weak var logo: UIImageView!
    
    // use for social
    
    var userFirstName = String()
    var userLastName = String()
    var userSocialId = String()
    var userSocialEmail = String()
    var profileImageStr = String()

    
    var fontsze : CGFloat = 13.0//= (DeviceType.IS_IPAD ? 16.0 : 13.0)
    
    private var indexToredirect : Int?
//    private var sideMenuObj = MenuTableView()
    
    // MARK: Start
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if UIApplication.sharedApplication().keyWindow?.rootViewController?.isKindOfClass(SideMenuNavigation) == true {
//            sideMenuObj = sideMenuController()!.sideMenu!.menuViewController as! MenuTableView
//            if sideMenuObj.lastSelectedMenuItem == 4
//            {
//                indexToredirect = (sideMenuController()?.sideMenu?.menuViewController as! MenuTableView).lastSelectedMenuItem
//            }
//        }
    }
    

    override func viewWillAppear(animated: Bool) {
//        if Constants.appDelegate.justLoggedIn == true && Constants.appDelegate.isRateView == true
//        {
//            Constants.appDelegate.isRateView = false
//            Constants.appDelegate.isRecipeDetail = false
//            sideMenuObj.UpdateMenuView()
//            self.dismissViewControllerAnimated(false, completion: nil)
//        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        
        return true
    }

    
    
    @IBAction func SignInPressed(sender: AnyObject) {
        
        dispatch_async(dispatch_get_main_queue(), {
            let login = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView") as! LoginView
            self.presentViewController(login, animated: true, completion: nil)
        })
    }
    
    @IBAction func RegisterPressed(sender: AnyObject) {
        
        dispatch_async(dispatch_get_main_queue(), {
            let login = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RegisterationView") as! RegisterationView
            self.presentViewController(login, animated: true, completion: nil)
        })
    }
    @IBAction func TryAppPressed(sender: AnyObject) {
        
//        Constants.appDelegate.justLoggedIn = false
//        
//        if  Constants.appDelegate.isRateView == true
//        {
//            sideMenuObj.UpdateMenuView()
//            self.dismissViewControllerAnimated(true, completion: nil)
//        }
//        else
//        {
//            Constants.appDelegate.isRecipeDetail = false
        
        
//        let customLoginView = UIView()
//        customLoginView.frame = CGRectMake(0, self.view.frame.size.height-120, self.view.frame.size.width, 120)
//        customLoginView.backgroundColor = UIColor.redColor()
//        
//        let alertview = NSBundle.mainBundle().loadNibNamed("LoginAlertView", owner: self, options: nil).first as? UIView
//        customLoginView.addSubview(alertview!)
//        Constants.appDelegate.window?.addSubview(customLoginView)
        
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "LoginRequiredError")
            let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
            UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
//        }
    }
    
    //MARK: - FACEBOOK
    
    @IBAction func FaceBookLoginPressed(sender: AnyObject) {
        
        if Reachability.isConnectedToNetwork() {
            
            Constants.appDelegate.startIndicator()
            dispatch_async(dispatch_get_main_queue())
            {
                let logManager = FBSDKLoginManager.init()
                //[FBSDKSettings sdkVersion]

                logManager.loginBehavior = FBSDKLoginBehavior.Web
                logManager.logOut()
                logManager.logInWithReadPermissions(["public_profile", "email"], fromViewController: self) { (result, error) in
                    if error != nil {
                        Constants.appDelegate.stopIndicator()
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title": "Too Shy To Ask", "message" : "Some error Occurred !"])
                    }
                    else if result.isCancelled == true {
                        Constants.appDelegate.stopIndicator()
                    }
                    else {
                        if result.token.tokenString != "" || result.token != nil {
                            self.fetchUserInfo()
                        }
                    }
                }
            }
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    func fetchUserInfo()
    {
        
        if Reachability.isConnectedToNetwork() {
            
            let req = FBSDKGraphRequest.init(graphPath: "me" , parameters: ["fields": "id, name, picture.width(500).height(500), email, gender"])
            dispatch_async(dispatch_get_main_queue())
            {
                req.startWithCompletionHandler
                    { (connection, result, error) in
                        if(error == nil) {
                            
                            let resDict = result as! Dictionary<String,AnyObject>
                            self.checkUserAuthonticationWithUserInfo(resDict)
                        }
                        else {
                            Constants.appDelegate.stopIndicator()
                            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Too Shy To Ask","message":"Some error occured"])
                        }
                }
            }
        }
        else {
             Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
    
    func checkUserAuthonticationWithUserInfo(result : NSDictionary) {
        
        if Reachability.isConnectedToNetwork() {

            let userId = result["id"] as! String
            userSocialId = userId
            let fullName = result["name"] as! String
            
            var nameArr = fullName.componentsSeparatedByString(" ")
            var lastname = nameArr.last
            if lastname == "" || lastname == nil {
                lastname = ""
            }
            else
            {
                userLastName = lastname!
            }
            
            nameArr.removeLast()
            let fname = nameArr.joinWithSeparator(" ")
             userFirstName = fname
            
            var email = result["email"] as? String
            
            if email == nil {
                email = ""
            }
            
            
            let picDic = result["picture"] as! Dictionary<String,AnyObject>
            let picData = picDic["data"] as! Dictionary<String,AnyObject>
            let profileImage = picData["url"] as! String
            profileImageStr = profileImage

            Constants.appDelegate.startIndicator()
            Server.LoginToApp(email!, authType: "1", authId: userSocialId, pass: "", completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                    Constants.appDelegate.stopIndicator()
                    let dict = response["data"]
                    NSUserDefaults.standardUserDefaults().setObject(dict!["user_id"], forKey:Constants.UserID)
                    NSUserDefaults.standardUserDefaults().setObject(dict!, forKey:Constants.PROFILE)
                    let passKey = dict!["pwd_blank"]
                    NSUserDefaults.standardUserDefaults().setObject("notHavePassword", forKey: Constants.showPassword)
                    NSUserDefaults.standardUserDefaults().synchronize()
                    if passKey! == nil
                    {
                        NSUserDefaults.standardUserDefaults().setObject(NSNumber.init(int: 1), forKey:Constants.PASSWORD)
                    }
                    else
                    {
                        NSUserDefaults.standardUserDefaults().setObject(passKey, forKey:Constants.PASSWORD)
                    }

                    dispatch_async(dispatch_get_main_queue(),
                        {
                            let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation") as! UINavigationController
                            UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
                    })
                }
                else {
                    // go to registeration
                    Constants.appDelegate.stopIndicator()

                    dispatch_async(dispatch_get_main_queue())
                    {
                    if email == ""
                    {
                        let emailId = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("EmailIdVC") as! EmailIdVC
                        emailId.sendEmailDelegate = self
                        emailId.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
                        self.presentViewController(emailId, animated: true, completion: nil)
                   

                    }
                    else
                    {
                        Constants.appDelegate.startIndicator()

                    Server.RegisterationForApp(fname, lname: lastname!,email: email!, mobile: "", authType: "1", authId: self.userSocialId, pass: "", profileImage: profileImage, completionHandler: { (response) in
                        
                        Constants.appDelegate.stopIndicator()
                        if response[Constants.STATE] as! Int == 1 {
                            let dict = response["data"]
                        
                            NSUserDefaults.standardUserDefaults().setObject(dict!["user_id"], forKey:Constants.UserID)
                            NSUserDefaults.standardUserDefaults().setObject(dict!, forKey:Constants.PROFILE)
                            let passKey = dict!["password"]
                            
                            if passKey! == nil
                            {
                                NSUserDefaults.standardUserDefaults().setObject(NSNumber.init(int: 1), forKey:Constants.PASSWORD)
                            }
                            else
                            {
                                NSUserDefaults.standardUserDefaults().setObject(passKey, forKey:Constants.PASSWORD)
                            }
                            
                             dispatch_async(dispatch_get_main_queue(),
                                {
                                    let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation") as! UINavigationController
                                    UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
                            })
                        }
                        else {
                             Constants.appDelegate.stopIndicator()
                            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":response["message"]!])
                        }
                    })
                    }
                }
                }
                }
            })
        }
        else {
            
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- DELEGATE EMAILID
    
    func getEmailIdStr(emailStr : String)
    {
        Constants.appDelegate.startIndicator()
        Server.RegisterationForApp(userFirstName, lname: userLastName,email: emailStr, mobile: "", authType: "1", authId: userSocialId, pass: "", profileImage: profileImageStr, completionHandler: { (response) in
            
            Constants.appDelegate.stopIndicator()
            if response[Constants.STATE] as! Int == 1 {
                let dict = response["data"]
                
                NSUserDefaults.standardUserDefaults().setObject(dict!["user_id"], forKey:Constants.UserID)
                NSUserDefaults.standardUserDefaults().setObject(dict!, forKey:Constants.PROFILE)
                let passKey = dict!["password"]
                
                if passKey! == nil
                {
                    NSUserDefaults.standardUserDefaults().setObject(NSNumber.init(int: 1), forKey:Constants.PASSWORD)
                }
                else
                {
                    NSUserDefaults.standardUserDefaults().setObject(passKey, forKey:Constants.PASSWORD)
                }
                
                dispatch_async(dispatch_get_main_queue(),
                    {
                        let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation") as! UINavigationController
                        UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
                })
            }
            else {
                Constants.appDelegate.stopIndicator()
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":response["message"]!])
            }
        })
    }
}
