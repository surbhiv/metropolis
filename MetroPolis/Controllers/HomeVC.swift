//
//  HomeVC.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 22/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
    
    override public var description: String {
        let id = identifier ?? ""
        return "id: \(id), constant: \(constant)" //you may print whatever you want here
    }
}

class HomeVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var timer = NSTimer()
    var collectionArray = [AnyObject]()
    var tableViewArray = [AnyObject]()
    var sliderCollectionArray = [AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getSliderDescription()
        let dict1 = ["heading" : "Information Storehouse", "subHeading" : "Accurate information at your fingertips" , "image" : "Health_Tips"]
        let dict2 = ["heading" : "Ask The Expert", "subHeading" : "Free Doctor Advice", "image" : "Ask_The_Expert"]
        let dict3 = ["heading" : "Quiz", "subHeading" : "Test your knowledge!" , "image" : "Gaming_Zone"]

        tableViewArray.append(dict1)
        tableViewArray.append(dict2)
        tableViewArray.append(dict3)
        
        homeTableView.reloadData()
        collectionView.reloadData()
    }

    override func viewWillDisappear(animated: Bool) {
        removeTimer()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        addTimer()
    }
    
    //MARK:- Get category API
    func getSliderDescription() {
        if Reachability.isConnectedToNetwork()
        {
            // NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)
            let method = "users/dashboard_slider"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
        
                
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                                        
                    self.sliderCollectionArray = response["data"]as! NSArray as [AnyObject]
                   dispatch_async(dispatch_get_main_queue())
                   {
                    self.collectionView.reloadData()

                    }
                    
                    
                    
                }
                else
                {
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let alert = UIAlertController.init(title: "Too Shy To Ask", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    
                    
                }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- COLLECTION VIEW
    //MARK:- DELEGATE
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSizeMake((ScreenSize.SCREEN_WIDTH - 20),125 )
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return sliderCollectionArray.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //MARK:- COLLECTION DATASOURSE
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("HomeHeaderCollectionCell", forIndexPath: indexPath)
        let headingLbl = cell.viewWithTag(241)as! UILabel
        headingLbl.text = sliderCollectionArray[indexPath.row]["title"]as? String
        pageControl.currentPage = indexPath.row
        let headingDescription = cell.viewWithTag(242)as! UILabel
        headingDescription.text = sliderCollectionArray[indexPath.row]["content"]as? String
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    }
    
    
    //MARK: - AUTO SCROLLING
    func addTimer()
    {
        timer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: #selector(nextPage), userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(timer, forMode: NSRunLoopCommonModes)
    }
    
    func nextPage()
    {
        let ar = collectionView.indexPathsForVisibleItems()
        if sliderCollectionArray.count > 0
        {
        let MaxSections = sliderCollectionArray.count
        var currentIndexPathReset = ar.first
        if(currentIndexPathReset?.item == MaxSections - 1)
        {
            currentIndexPathReset = NSIndexPath(forItem:0, inSection:0)
            pageControl.currentPage = 0
            collectionView.scrollToItemAtIndexPath(currentIndexPathReset!, atScrollPosition: UICollectionViewScrollPosition.Left, animated: false)
        }
        else
        {
            if currentIndexPathReset != nil
            {
            let newIndex = currentIndexPathReset!.item + 1
            pageControl.currentPage = newIndex
            collectionView.scrollToItemAtIndexPath(NSIndexPath.init(forItem: newIndex, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
            }
        }
        }
        
    }
    
    func removeTimer()
    {
        // stop NSTimer
        timer.invalidate()
    }
    
    // UIScrollView' delegate method
    func scrollViewWillBeginDragging(scrollView: UIScrollView)
    {
        removeTimer()
    }
    
    
    func scrollViewDidEndDragging(scrollView: UIScrollView,
                                  willDecelerate decelerate: Bool)
    {
        addTimer()
    }

    
    //MARK:- TABLEVIEW
    //TABLEVIEW DELEGATE
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewArray.count
    }
    
    //TABLEVIEW DATASOURse
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("homeHeaderTableViewCell", forIndexPath: indexPath)
        let headingImage = cell.contentView.viewWithTag(1001)as! UIImageView
        let headingLbl = cell.contentView.viewWithTag(1002)as! UILabel
        let subHeadingLbl = cell.contentView.viewWithTag(1003)as! UILabel
        headingLbl.text = tableViewArray[indexPath.row]["heading"]as? String
        subHeadingLbl.text = tableViewArray[indexPath.row]["subHeading"]as? String
        headingImage.image = UIImage.init(named: tableViewArray[indexPath.row]["image"]as! String)
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let topview = self.parentViewController as! DashBoardVC
        let btn : UIButton
        btn = UIButton.init()
        
        if indexPath.row == 0 {
            let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("HealthTipsVC")as! HealthTipsVC
            destViewController.isNotFromSideMenu = true
            self.navigationController?.pushViewController(destViewController, animated: true)
        }
        else if indexPath.row == 1{
            btn.tag = 1
            topview.HeaderlabelSelected(btn)
        }
        else{
            
            btn.tag = 2
            topview.HeaderlabelSelected(btn)
        }
    }

    

}
