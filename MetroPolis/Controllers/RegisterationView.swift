//
//  RegisterationView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 26/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
//import Firebase

class RegisterationView: UIViewController, UITextFieldDelegate {

    // MARK: IBOutlets Defined
    @IBOutlet weak var logoText: UIImageView!
    @IBOutlet weak var logoHeight: NSLayoutConstraint!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var lineName: UILabel!
    @IBOutlet weak var nameHeight: NSLayoutConstraint!

    @IBOutlet weak var lnameLabel: UILabel!
    @IBOutlet weak var lnameText: UITextField!
    @IBOutlet weak var llineName: UILabel!
    @IBOutlet weak var lnameHeight: NSLayoutConstraint!

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var lineEmail: UILabel!
    @IBOutlet weak var emailHeight: NSLayoutConstraint!

    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var mobileText: UITextField!
    @IBOutlet weak var lineMobile: UILabel!
    @IBOutlet weak var mobileHeight: NSLayoutConstraint!

    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var linePass: UILabel!
    @IBOutlet weak var passHeight: NSLayoutConstraint!

    @IBOutlet weak var confirmPassLabel: UILabel!
    @IBOutlet weak var confirmPassText: UITextField!
    @IBOutlet weak var lineConfirmPass: UILabel!
    @IBOutlet weak var confirmPassHeight: NSLayoutConstraint!

    @IBOutlet weak var termsAcceptButton: UIButton!

    var fontsze : CGFloat = (DeviceType.IS_IPAD ? 16.0 : 13.0)

    
    var authType: Int = 0
    var indexToredirect : Int?
    
    
    // MARK: Start
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        UIColor.init(red: 40.0/255.0, green: 224.0/255.0, blue: 42.0/255.0, alpha: 1.0)
        nameText.attributedPlaceholder = NSAttributedString(string: "First Name*",  attributes: [NSForegroundColorAttributeName : UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1), NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
        lnameText.attributedPlaceholder = NSAttributedString(string: "Last Name",  attributes: [NSForegroundColorAttributeName : UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1), NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])

        emailText.attributedPlaceholder = NSAttributedString(string: "Email id*",  attributes: [NSForegroundColorAttributeName : UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1), NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
        mobileText.attributedPlaceholder = NSAttributedString(string: "Mobile No",  attributes: [NSForegroundColorAttributeName : UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1), NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
        passText.attributedPlaceholder = NSAttributedString(string: "Password*",  attributes: [NSForegroundColorAttributeName : UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1), NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
        confirmPassText.attributedPlaceholder = NSAttributedString(string: "Confirm Password*",  attributes: [NSForegroundColorAttributeName : UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1), NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
        
    }
    

    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == nameText {
            ACTIVATETextField(nameText, textLabel: nameLabel, line: lineName, height: nameHeight)
        }
        else if textField == lnameText {
            ACTIVATETextField(lnameText, textLabel: lnameLabel, line: llineName, height: lnameHeight)
        }

        else if textField == emailText {
            ACTIVATETextField(emailText, textLabel: emailLabel, line: lineEmail, height: emailHeight)
        }
        else if textField == mobileText {
            ACTIVATETextField(mobileText, textLabel: mobileLabel, line: lineMobile, height: mobileHeight)
        }
        else if textField == passText {
            ACTIVATETextField(passText, textLabel: passLabel, line: linePass, height: passHeight)
        }
        else if textField == confirmPassText {
            ACTIVATETextField(confirmPassText, textLabel: confirmPassLabel, line: lineConfirmPass, height: confirmPassHeight)
        }
    }
    
    func ACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
         UIView.animateWithDuration(1.0, delay: 0, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: {
            
            textLabel.hidden = false
            textf.attributedPlaceholder = NSAttributedString(string: "")
            Helper.activatetextField(line,height: height)

            },
            completion: { (true) in
                textLabel.hidden = false
         })
    }
    
    
    func DEACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        if textf.text == ""
        {
            UIView.animateWithDuration(1.0, animations: {
                textLabel.hidden = true
                
                Helper.deActivatetextField(line,height: height)
                
                textf.attributedPlaceholder = NSAttributedString(string: textLabel.text! ,  attributes: [NSForegroundColorAttributeName : UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1), NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
                }, completion: nil)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == nameText {
            DEACTIVATETextField(nameText, textLabel: nameLabel, line: lineName, height: nameHeight)
        }
        else if textField == lnameText {
            DEACTIVATETextField(lnameText, textLabel: lnameLabel, line: llineName, height: lnameHeight)
        }
        else if textField == emailText {
            DEACTIVATETextField(emailText, textLabel: emailLabel, line: lineEmail, height: emailHeight)
        }
        else if textField == mobileText {
            DEACTIVATETextField(mobileText, textLabel: mobileLabel, line: lineMobile, height: mobileHeight)
        }
        else if textField == passText {
            DEACTIVATETextField(passText, textLabel: passLabel, line: linePass, height: passHeight)
        }
        else if textField == confirmPassText {
            DEACTIVATETextField(confirmPassText, textLabel: confirmPassLabel, line: lineConfirmPass, height: confirmPassHeight)
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileText {
            
            if (range.location > 9)
            {
                return false
            }
        }
        
        return true
    }

    // MARK: BUTTON ACTIONS
    
    @IBAction func OpenTermsandConditionView(sender: AnyObject) {
        
//        let dest = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("TermsConditionVC") as! TermsConditionVC
//        dest.isComingFromSideMenu = false
//        self.presentViewController(dest, animated: true, completion: nil)
    }
    
    @IBAction func TermsSelectionButtonPressed(sender: AnyObject) {
        if termsAcceptButton.selected {
            termsAcceptButton.selected = false
        }
        else{
            termsAcceptButton.selected = true
        }
    }
    
    @IBAction func PrivacyButtonPressed(sender: AnyObject) {
        
        let destVC = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("TermAndPolicyWebVC")as! TermAndPolicyWebVC
        destVC.isTerm = false
        destVC.modalInPopover = true

        self.presentViewController(destVC, animated: true) {
        }
    }
    
    @IBAction func TermsAndConditionButtonPressed(sender: AnyObject) {

        let destVC = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("TermAndPolicyWebVC")as! TermAndPolicyWebVC
        destVC.isTerm = true
        destVC.modalInPopover = true

        self.presentViewController(destVC, animated: true) {
        }
    }
    
    @IBAction func GoToLoginViewPressed(sender: AnyObject) {
        nameText.resignFirstResponder()
        passText.resignFirstResponder()
        emailText.resignFirstResponder()
        confirmPassText.resignFirstResponder()
        lnameText.resignFirstResponder()
        mobileText.resignFirstResponder()

        
        if ((self.presentingViewController?.isKindOfClass(LoginView)) == true){
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else{
            dispatch_async(dispatch_get_main_queue(), {
                let login = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView") as! LoginView
                self.presentViewController(login, animated: true, completion: nil)
            })
        }
    }
    
    @IBAction func TryAppPressed(sender: AnyObject) {
        nameText.resignFirstResponder()
        lnameText.resignFirstResponder()
        passText.resignFirstResponder()
        emailText.resignFirstResponder()
        confirmPassText.resignFirstResponder()
        mobileText.resignFirstResponder()
        
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "LoginRequiredError")
        let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
        UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    
    //MARK: API BUTTON ACTIONS
    @IBAction func SignUpPressed(sender: AnyObject) {
        
        nameText.resignFirstResponder()
        passText.resignFirstResponder()
        emailText.resignFirstResponder()
        confirmPassText.resignFirstResponder()
        
        
        if nameText.text == "" {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter User Name !"])
        }
        else if emailText.text == ""{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter Email Id !"])
        }
        else if !Helper.isValidEmail(emailText.text!){
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter Valid Email Id !"])
        }
        else if passText.text == ""{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter Password !"])
        }
        else if passText.text?.characters.count < 6
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter minimum 6 characters Password !"])
        }
        else if confirmPassText.text == ""{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter Confirm Password !"])
        }
        else if confirmPassText.text != passText.text{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Confirm Password does not match."])
        }
        else if termsAcceptButton.selected == false{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Please agree with terms & conditions"])
        }
        else {
            if mobileText.text == ""{
                
                self.RegisterMethodCalled()
            }
            else
            {
                if !Helper.isValidPhoneNumber(mobileText.text!) {
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter Valid Mobile No !"])
                }
                else
                {
                    self.RegisterMethodCalled()
                }
            }
        }
    }
    
    
    func RegisterMethodCalled()
    {
        if Reachability.isConnectedToNetwork() {
            
            let nameStr = nameText.text! as String
            let lastname = lnameText.text! as String
            Constants.appDelegate.startIndicator()
            Server.RegisterationForApp(nameStr,lname : lastname, email: emailText.text!, mobile: mobileText.text!, authType: "", authId: "", pass: passText.text!, profileImage: "", completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] as! Int == 1 {
                    let dict = response["data"]
                    
                    NSUserDefaults.standardUserDefaults().setObject(dict!["user_id"], forKey:Constants.UserID)
                    NSUserDefaults.standardUserDefaults().setObject(dict!, forKey:Constants.PROFILE)
                    let passKey = dict!["password"]
                    
                    if passKey! == nil
                    {
                        NSUserDefaults.standardUserDefaults().setObject(NSNumber.init(int: 11), forKey:Constants.PASSWORD)
                    }
                    else
                    {
                        NSUserDefaults.standardUserDefaults().setObject(passKey, forKey:Constants.PASSWORD)
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation") as! UINavigationController
                        UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
                    })
                }
                else {
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }
            })
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    
//    func firebaseAnalyticMethod()
//    {
//        FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
//            kFIRParameterItemID:"8",
//            kFIRParameterItemName:"Register",
//            kFIRParameterContentType:"Registeration Success"
//            ])
//    }
}
