//
//  ChatWithExpertVC.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 26/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ChatWithExpertVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {

    @IBOutlet weak var chatTable: UITableView!
    @IBOutlet weak var replyTextView: UITextView!
    @IBOutlet weak var replyBtn: UIButton!
    @IBOutlet weak var replyViewHeightConstraint: NSLayoutConstraint!
    
    var parentQuestionID = String()
    var myChatDataArray = [AnyObject]()
    var replyDataArray = [AnyObject]()
    
    var timer = NSTimer()
    
    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllChatDataRequest(true)

        // Do any additional setup after loading the view.
        self.addTimer()
    }
    override func viewWillDisappear(animated: Bool) {
        removeTimer()
    }
    func addTimer()
    {
        timer = NSTimer.scheduledTimerWithTimeInterval(30, target: self, selector: #selector(getBackgroundData), userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(timer, forMode: NSRunLoopCommonModes)
    }
    
    func removeTimer()
    {
        // stop NSTimer
        timer.invalidate()
    }

    func getBackgroundData() {
        getAllChatDataRequest(false)
        
    }
    
    //MARK:- GET MY QUESTION CHAT LIST API
    func getAllChatDataRequest(showLoader : Bool)
    {
        if Reachability.isConnectedToNetwork()
        {
            // NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)
            let method = "users/my_question_replies?user_id=\(NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)!)&question_id=\(parentQuestionID)"
            if showLoader == true
            {
            Constants.appDelegate.startIndicator()
            }
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                    let tempDataDict  = response["data"]
                    if self.myChatDataArray.count > 0
                    {
                        self.myChatDataArray.removeAll()
                    }
                     self.myChatDataArray.append(tempDataDict!)
                    if self.myChatDataArray[0]["replies"] != nil
                    {
                        if (self.myChatDataArray[0]["replies"]??.isKindOfClass(NSArray) == true)
                        {
                        
                        self.replyDataArray = self.myChatDataArray[0]["replies"]as! NSArray as [AnyObject]
                        }
                    }
                    //print(self.myChatDataArray)
                    dispatch_async(dispatch_get_main_queue())
                    {
                        self.performSelector(#selector(self.reloadTable), withObject: self, afterDelay: 0.5)
                        self.chatTable.reloadData()
                    }
                    
                    
                }
                else
                {
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let alert = UIAlertController.init(title: "Too Shy To Ask", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    
                    
                }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }

    }
    
    func reloadTable()
    {
    
        dispatch_async(dispatch_get_main_queue())
        {
        
            self.chatTable.reloadData()
            let totalcount = self.replyDataArray.count + self.myChatDataArray.count
            self.chatTable.scrollToRowAtIndexPath(NSIndexPath.init(forRow: 0, inSection: totalcount-1), atScrollPosition: .Bottom, animated: false)
        }
    }
    
    //MARK:- Post method for reply
    
    //MARK:- GET MY QUESTION CHAT LIST API
    func replyPostAPI()
    {
        if Reachability.isConnectedToNetwork()
        {
            // NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)
            let method = "users/question_reply"
            var replyText = replyTextView.text
            replyText = replyText.stringByReplacingOccurrencesOfString("&", withString: "##and")

            var param = "user_id=\(NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)!)&question_id=\(parentQuestionID)&question=\(replyText)"
            param = param.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                    dispatch_async(dispatch_get_main_queue())
                    {
                        self.replyTextView.text = "Type a Message here"
                        self.getAllChatDataRequest(true)
                    }
                    
                }
                else
                {
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        let alert = UIAlertController.init(title: "Too Shy To Ask", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    
                    
                }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if replyDataArray.count != 0
        {
           return myChatDataArray.count + replyDataArray.count
        }
        else
        {
        return myChatDataArray.count
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if section == 0
//        {
//            return myChatDataArray.count
//        }
//        else
//        {
//            return replyDataArray.count
//        }
        if myChatDataArray.count != 0
        {
            return 1
        }
        else
        
        {
            return 0
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {

       
        /*
        let CellIdentifier = "Bubble Cell"
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier, forIndexPath: indexPath)as! STBubbleTableViewCell
        
        cell.backgroundColor = tableView.backgroundColor
        cell.selectionStyle = .None
        
        
        cell.dataSource = self;
        cell.delegate = self;
        
        tableView.estimatedRowHeight = 90
        tableView.rowHeight = UITableViewAutomaticDimension
    
        
        if indexPath.section == 0
        {
        if myChatDataArray[indexPath.section]["question_type"]as! String == "New"
        {
            let messageStr = myChatDataArray[indexPath.section]["question"]as? String
            var message = Message()
            
            if indexPath.row == 0
            {
                if myChatDataArray[indexPath.section]["question"] != nil
                {
                   
                    message = Message.init(string: messageStr)
                    cell.textLabel?.text = message.message
                    cell.authorType = .STBubbleTableViewCellAuthorTypeSelf
                    cell.bubbleColor = .STBubbleTableViewCellBubbleColorRed
                    cell.selectedBubbleColor = BubbleColor.STBubbleTableViewCellBubbleColorBrown
                }
            }
//            else
//            {
//                if myChatDataArray[indexPath.section]["reply"] != nil
//                {
//                    message = Message.init(string: messageStr)
//                    cell.textLabel?.text = message.message
////                    cell.imageView?.image = UIImage.init(named: "profilePlaceHolder")
////                    cell.imageView?.contentMode = .ScaleAspectFit
//                    cell.authorType = .STBubbleTableViewCellAuthorTypeOther;
//                    cell.bubbleColor = .STBubbleTableViewCellBubbleColorGreen
//                }
//            }
        }
        }
        else
        {
            let messageStr = replyDataArray[indexPath.section - 1]["reply"]as? String
            var message = Message()

            
            message = Message.init(string: messageStr)
            cell.textLabel?.text = message.message
            if replyDataArray[indexPath.row]["user_id"]as? String == "0"
            {
                cell.authorType = .STBubbleTableViewCellAuthorTypeOther
            }
            else if replyDataArray[indexPath.row]["expert_id"]as? String == "0"
            {
                cell.authorType = .STBubbleTableViewCellAuthorTypeSelf
            }
            
            cell.bubbleColor = .STBubbleTableViewCellBubbleColorYellow
        }
        
        
        return cell
 */
 
//        Message *message = [self.messages objectAtIndex:indexPath.row];
//        
////        cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
//        cell.textLabel.text = message.message;
//        cell.imageView.image = message.avatar;
        
        // Put your own logic here to determine the author
//        if(indexPath.row % 2 != 0 || indexPath.row == 4)
//        {
//            cell.authorType = STBubbleTableViewCellAuthorTypeSelf;
//            cell.bubbleColor = STBubbleTableViewCellBubbleColorGreen;
//        }
//        else
//        {
//            cell.authorType = STBubbleTableViewCellAuthorTypeOther;
//            cell.bubbleColor = STBubbleTableViewCellBubbleColorGray;
//        }

        tableView.estimatedRowHeight = 110
        tableView.rowHeight = UITableViewAutomaticDimension
        let cell = tableView.dequeueReusableCellWithIdentifier("QuestionTypeCell", forIndexPath: indexPath)as! QuestionTypeCell
        cell.selectionStyle = .None
        
        if indexPath.section == 0
        {
            if myChatDataArray[indexPath.section]["question_type"]as! String == "New"
            {
            
                if indexPath.row == 0
                {
                    if myChatDataArray[indexPath.section]["question"] != nil
                    {
                        cell.replyLbl.text = myChatDataArray[indexPath.section]["question"]as? String
                       // [[UIImage imageNamed:@"bubbleSomeone.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:14]
                        cell.chatBgImage.image = UIImage.init(named: "bubbleMine.png")?.stretchableImageWithLeftCapWidth(21, topCapHeight: 14)
                    }
//
               

                
                
            }
            
            
            
        }
            return cell
        }
        else
        {
           let cell = tableView.dequeueReusableCellWithIdentifier("AnswerTypeCell", forIndexPath: indexPath)as! AnswerTypeCell
            cell.selectionStyle = .None
            var dateStr = String()
            if replyDataArray[indexPath.section - 1]["created"] != nil
            {
                dateStr = Helper.getStringFromDate(replyDataArray[indexPath.section - 1]["created"]as! String)
            }

            if replyDataArray[indexPath.section - 1]["user_id"]as? String == "0"
            {
                
                if replyDataArray[indexPath.section - 1]["reply"] != nil
                {
                    cell.replyLbl.text = replyDataArray[indexPath.section - 1]["reply"]as? String
                    cell.dateLbl.text = dateStr
                    cell.chatBgImage.image = UIImage.init(named: "bubbleSomeone.png")?.stretchableImageWithLeftCapWidth(15, topCapHeight: 14)
                }
               
                
                return cell
            }
            else if replyDataArray[indexPath.section - 1]["expert_id"]as? String == "0"
            {
                let cell = tableView.dequeueReusableCellWithIdentifier("QuestionTypeCell", forIndexPath: indexPath)as! QuestionTypeCell
                cell.selectionStyle = .None
                if replyDataArray[indexPath.section - 1]["reply"] != nil
                {
                    cell.replyLbl.text = replyDataArray[indexPath.section - 1]["reply"]as? String
                    cell.dateLbl.text = dateStr
                    cell.chatBgImage.image = UIImage.init(named: "bubbleMine.png")?.stretchableImageWithLeftCapWidth(21, topCapHeight: 14)
                }
                
                
                return cell
            }
            
            
           
            return cell

            
        }
 
      }
        
   //MARK:- TextviewDelegate
    
    func textViewDidBeginEditing(textView: UITextView) {
        
        textView.text = ""
    }
    func textViewDidEndEditing(textView: UITextView) {
        
        if textView.text.characters.count == 0
        
        {
            textView.text = "Type a Message here"
        }
    }
    @IBAction func backBtnClick(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func replyBtnClicked(sender: AnyObject) {
        replyTextView.resignFirstResponder()
        
        if replyTextView.text == "" || replyTextView.text == "Type a Message here"
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Too Shy To Ask", "message": "Please reply with message"] )

        }
        else
        {
            self.replyPostAPI()
        }
    }
    @IBAction func sideMenuClick(sender: AnyObject) {
        toggleSideMenuView()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
