//
//  LoginView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 26/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

//import Firebase
//import Google
//import FacebookCore
//import FacebookLogin


class LoginView: UIViewController, UITextFieldDelegate {

    // MARK: IBOutlets Defined
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var emailLine: UILabel!
    @IBOutlet weak var lineHeight: NSLayoutConstraint!
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var passLine: UILabel!
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var passLineHeight: NSLayoutConstraint!
    @IBOutlet weak var termsAcceptButton: UIButton!

    var fontsze : CGFloat = (DeviceType.IS_IPAD ? 16.0 : 14.0)
    
    @IBOutlet weak var viewHeightConst: NSLayoutConstraint!
    private var indexToredirect : Int?
//    private var sideMenuObj = MenuTableView()
    
    // MARK: Start
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DeviceType.IS_IPHONE_6{
            viewHeightConst.constant = 650
        }
        else if DeviceType.IS_IPHONE_6P{
            viewHeightConst.constant = 700
        }
        
        
        dispatch_async(dispatch_get_main_queue()) {
            // Do stuff to UI
            self.emailText.attributedPlaceholder = NSAttributedString(string: " Email Id *",  attributes: [NSForegroundColorAttributeName : UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1), NSFontAttributeName : UIFont(name: Fonts.regFONTname, size: self.fontsze)!])
            
            self.passText.attributedPlaceholder = NSAttributedString(string: " Password *",  attributes: [NSForegroundColorAttributeName : UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1), NSFontAttributeName : UIFont(name: Fonts.regFONTname, size: self.fontsze)!])
        }
    }
    

    override func viewWillAppear(animated: Bool) {
//        if Constants.appDelegate.justLoggedIn == true && Constants.appDelegate.isRateView == true
//        {
//            Constants.appDelegate.isRateView = false
//            Constants.appDelegate.isRecipeDetail = false
//            sideMenuObj.UpdateMenuView()
//            self.dismissViewControllerAnimated(false, completion: nil)
//        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        
        return true
    }

    
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == emailText {

            UIView.animateWithDuration(1.0, animations: {
                self.emailLabel.hidden = false
                self.emailText.attributedPlaceholder = NSAttributedString(string: "")
                Helper.activatetextField(self.emailLine,height: self.lineHeight)

                },
                completion: { (true) in
                    dispatch_async(dispatch_get_main_queue(), {
                        self.emailLabel.hidden = false
                        self.view.bringSubviewToFront(self.emailLabel)
                        self.emailText.attributedPlaceholder = NSAttributedString(string: "")
                        Helper.activatetextField(self.emailLine,height: self.lineHeight)
                        
                        if self.passText.text == nil || self.passText.text == "" {
                            self.passLabel.hidden = true
                        }
                        else {
                            self.passLabel.hidden = false
                        }
                    })
            })
            
            
        }
        if textField == passText {
            if emailText.text == nil || emailText.text == "" {
             emailLabel.hidden = true
            }
            else {
                emailLabel.hidden = false
            }
            UIView.animateWithDuration(1.0, delay: 0, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: {
                self.passLabel.hidden = false
                self.passText.attributedPlaceholder = NSAttributedString(string: "")
                Helper.activatetextField(self.passLine,height: self.passLineHeight)
                }, completion: nil)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == emailText {
            if textField.text == nil || textField.text == ""
            {
                UIView.animateWithDuration(1.0, animations: {
                    self.emailLabel.hidden = true
                    Helper.deActivatetextField(self.emailLine,height: self.lineHeight)
                    self.emailText.attributedPlaceholder = NSAttributedString(string: " Email Id *",  attributes: [NSForegroundColorAttributeName : UIColor.blackColor(), NSFontAttributeName : UIFont(name: Fonts.regFONTname, size: self.fontsze)!])
                    }, completion: nil)
            }
        }
        if textField == passText {
            if textField.text == nil || textField.text == ""
            {
                UIView.animateWithDuration(1.0, animations: {
                    self.passLabel.hidden = true
                    Helper.deActivatetextField(self.passLine,height: self.passLineHeight)
                    self.passText.attributedPlaceholder = NSAttributedString(string: " Password *",  attributes: [NSForegroundColorAttributeName : UIColor.blackColor(), NSFontAttributeName : UIFont(name: Fonts.regFONTname, size: self.fontsze)!])
                    }, completion: nil)
            }
        }

    }
    
    
    // MARK: BUTTON ACTIONS
    
    
    @IBAction func ForgotPasswordPressed(sender: AnyObject) {
        
        passText.resignFirstResponder()
        emailText.resignFirstResponder()

        let forgot = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("ForgotPassword") as! ForgotPasswordView
        forgot.isForgetView = true
        forgot.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        self.presentViewController(forgot, animated: true, completion: nil)

    }
    
    @IBAction func RegisterPressed(sender: AnyObject) {
        
        dispatch_async(dispatch_get_main_queue(), {
            let login = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RegisterationView") as! RegisterationView
            self.presentViewController(login, animated: true, completion: nil)
        })
    }
    
    
    @IBAction func TryAppPressed(sender: AnyObject) {
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "LoginRequiredError")
        let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
        UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
        
        

    }
    
    
    @IBAction func TermsSelectionButtonPressed(sender: AnyObject) {
        if termsAcceptButton.selected {
            termsAcceptButton.selected = false
        }
        else{
            termsAcceptButton.selected = true
        }
    }
    
    @IBAction func PrivacyButtonPressed(sender: AnyObject) {

        let destVC = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("TermAndPolicyWebVC")as! TermAndPolicyWebVC
        destVC.isTerm = false
        destVC.modalInPopover = true

        self.presentViewController(destVC, animated: true) {
        }
    }
    
    @IBAction func TermsAndConditionButtonPressed(sender: AnyObject) {
        
        let destVC = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("TermAndPolicyWebVC")as! TermAndPolicyWebVC
        destVC.isTerm = true
        destVC.modalInPopover = true
        
        self.presentViewController(destVC, animated: true) {
        }
    }
    
    
//    @IBAction func TryAppPressed(sender: AnyObject) {
//        
////        passText.resignFirstResponder()
////        emailText.resignFirstResponder()
////        Constants.appDelegate.justLoggedIn = false
////        
////        if  Constants.appDelegate.isRateView == true
////        {
////            sideMenuObj.UpdateMenuView()
////            self.dismissViewControllerAnimated(true, completion: nil)
////        }
////        else
////        {
////            Constants.appDelegate.isRecipeDetail = false
////            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "LoginRequiredError")
////            let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
////            UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
////        }
//    }
    
    
//    func firebaseAnalyticMethod(authType:String)
//    {
//        if authType == "1"
//        {
//            FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
//                kFIRParameterItemID:"9",
//                kFIRParameterItemName:"Facebook",
//                kFIRParameterContentType:"Facebook"
//                ])
//        }
//        else if authType == "2"
//        {
//            FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
//                kFIRParameterItemID:"10",
//                kFIRParameterItemName:"Google",
//                kFIRParameterContentType:"Google Plus"
//                ])
//
//        }
//        else
//        {
//            FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
//                kFIRParameterItemID:"7",
//                kFIRParameterItemName:"Login",
//                kFIRParameterContentType:"LogIn Success"
//                ])
//
//        }
//    }
    
    
    //MARK: API BUTTON ACTIONS
    @IBAction func SignInPressed(sender: AnyObject) {
        
        emailText.resignFirstResponder()
        passText.resignFirstResponder()
        
        
        if emailText.text == "" {
            
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message": "Email Id cannot be empty."] )
        }
        else if !Helper.isValidEmail(emailText.text!) {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message": "Enter a valid email id."] )
        }
        else if passText.text == "" {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message": "Password cannot be empty."] )
        }
        
        else if termsAcceptButton.selected == false{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Please agree with terms & conditions"])
        }
        else {
            if Reachability.isConnectedToNetwork() {
                Constants.appDelegate.startIndicator()
                
                Server.LoginToApp(emailText.text!, authType: "", authId: "", pass: passText.text!, completionHandler: { (response) in
                    
                    Constants.appDelegate.stopIndicator()
                    if response[Constants.STATE] as! Int == 1 {
                        
                        let dict = response["data"]
                        NSUserDefaults.standardUserDefaults().setObject(dict!["user_id"], forKey:Constants.UserID)
                        NSUserDefaults.standardUserDefaults().setObject(dict!, forKey:Constants.PROFILE)
                        let passKey = dict!["pwd_blank"]
                        
                        if passKey! == nil
                        {
                            NSUserDefaults.standardUserDefaults().setObject(NSNumber.init(int: 11), forKey:Constants.PASSWORD)// earlier we put value 0 but since now in login we get 0 im putting here 11 - so in edit profile while checking for passkey value check for 11 instead of 0
                        }
                        else
                        {
                            NSUserDefaults.standardUserDefaults().setObject(passKey, forKey:Constants.PASSWORD)
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            //                            Constants.appDelegate.justLoggedIn = true
                            //
                            //                            if  Constants.appDelegate.isRateView == true  || Constants.appDelegate.isRecipeDetail == true
                            //                            {
                            //                                self.sideMenuObj.UpdateMenuView()
                            //                                Constants.appDelegate.isRecipeDetail = false
                            //                                self.dismissViewControllerAnimated(true, completion: nil)
                            //                            }
                            //                            else
                            //                            {
                                                            let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
                                                            UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
                            //                            }
                        })
                    }
                    else {
                        let mess = response["message"] as! String
                        dispatch_async(dispatch_get_main_queue(), {
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":mess])
                        })
                    }
                })
            }
            else {
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
        }
    }

}
