//
//  TermAndPolicyWebVC.swift
//  MetroPolis
//
//  Created by Hitesh Dhawan on 12/06/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class TermAndPolicyWebVC: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var termWebView: UIWebView!
    
    var isTerm = Bool()
    var htmlStr = String()
    @IBAction func backPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) { 
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var method = String()
        if isTerm == true
        {
            titleLabel.text = "TERMS & CONDITIONS"
            method = "users/static_content?alias=terms-conditions"
        }
        else
        {
           titleLabel.text = "PRIVACY POLICY"
            method = "users/static_content?alias=privacy-policy"
        }
        let urlRequest = NSMutableURLRequest(URL: NSURL(string: Constants.BASEURL+method)!)
        urlRequest.HTTPMethod = "GET"
        
        // Handling Basic HTTPS Authorization
        let authString = "healthcare@gmail.com:Neuro@123"
        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)
        let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        termWebView.loadRequest(urlRequest)
        
        //self.getData()
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        
        Constants.appDelegate.startIndicator()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        
        Constants.appDelegate.stopIndicator()
        
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        print(error)
        Constants.appDelegate.stopIndicator()
    }

    
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }
