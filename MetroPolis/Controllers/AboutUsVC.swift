//
//  AboutUsVC.swift
//  MetroPolis
//
//  Created by Surbhi on 29/08/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController ,UIWebViewDelegate {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var aboutUSwebView: UIWebView!
    
    var htmlStr = String()
    
    //MARK:- SIDE MENU BTN
    @IBAction func sideMenuClick(sender: AnyObject) {
        toggleSideMenuView()
        
    }
    @IBAction func backPressed(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    //MARK:- DID Load
    override func viewDidLoad() {
        super.viewDidLoad()

        var method = String()

        method = "users/static_content?alias=about-us"
        let urlRequest = NSMutableURLRequest(URL: NSURL(string: Constants.BASEURL+method)!)
        urlRequest.HTTPMethod = "GET"
        
        // Handling Basic HTTPS Authorization
        let authString = "healthcare@gmail.com:Neuro@123"
        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)
        let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        aboutUSwebView.loadRequest(urlRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- WEbview
    func webViewDidStartLoad(webView: UIWebView) {
        
        Constants.appDelegate.startIndicator()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        
        Constants.appDelegate.stopIndicator()
        
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        print(error)
        Constants.appDelegate.stopIndicator()
    }



}
