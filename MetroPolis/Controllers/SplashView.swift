 //
//  SplashView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 26/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class SplashView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        Constants.appDelegate.registerForPush()
        GoToLogin()
    }
    
    
    func GoToLogin() {
        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""  {
            
            dispatch_async(dispatch_get_main_queue(), {
                
                let login = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LandingView")
                UIApplication.sharedApplication().keyWindow?.rootViewController = login
            })
        }
        else {
            dispatch_async(dispatch_get_main_queue(), {
                
                let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
                UIApplication.sharedApplication().keyWindow?.rootViewController = viewController
            })
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    

}
