//
//  AppDelegate.swift
//  MetroPolis
//
//  Created by Surbhi on 13/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var deviceTokenString = String()
    var activityView1  = UIView()
    var imageForR = UIImageView ()
    var load = UILabel()


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        if self.window == nil {
            self.window = UIWindow.init(frame: UIScreen.mainScreen().bounds)
        }
        let root =  Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SplashView") as! SplashView
        self.window?.rootViewController = root
//
        let notificationType: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        let pushNotificationSettings = UIUserNotificationSettings(forTypes: notificationType, categories: nil)
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        
        // Handle push notification's payload when user clicks on notification
        if let notification = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? [String: AnyObject] {
            savePushMessages(notification["aps"] as! [String: AnyObject])
        }
                
        self.createIndicator()
        self.window?.makeKeyAndVisible()
        
        return true
    }
    
    // MARK:- RegisterDevice Methods
    
    func registerDevice() {
        
        print("TOKEN --- \(AppDelegate.getDeviceToken())")
        
        if Reachability.isConnectedToNetwork() {
            
            let method = "users/device_registration"
            var param = "device_type=ios&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&device_version=\(UIDevice.currentDevice().systemVersion)&user_id=\(0)"
            
            if (NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)) != nil{
                param = "device_type=ios&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&device_version=\(UIDevice.currentDevice().systemVersion)&user_id=\((NSUserDefaults.standardUserDefaults().objectForKey(Constants.UserID)!))"
            }
            
            param = param.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!

            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param, completionHandler: { (response) in
                
                 if response[Constants.STATE] != nil
                {
                if response[Constants.STATE] as! Int == 1
                {
                    print("Register Device Response - \(response)")
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue())
                    {
                        self.registerDevice()
                    }
                }
                }
                
            })
            
            
        }
    }

    
    //MARK:- save notification message
    func savePushMessages(notificationDic:[String: AnyObject]) {
        
        
        
        
        var msgArray = NSMutableArray()
        let prefs = NSUserDefaults.standardUserDefaults()
        
        if let array = prefs.objectForKey("PUSH_MESSAGES") {
            msgArray = array.mutableCopy()!  as! NSMutableArray
        }
       
        msgArray.insertObject(notificationDic, atIndex: 0)
        prefs.setObject(msgArray, forKey: "PUSH_MESSAGES")

        var badgeValue: Int = prefs.integerForKey("BADGE_COUNT")
        badgeValue = badgeValue + 1
        prefs.setInteger(badgeValue, forKey: "BADGE_COUNT")
        
        prefs.synchronize()
        
        if ((window?.rootViewController?.isKindOfClass(SideMenuNavigation)) == true)
        {
            if ((window?.rootViewController?.childViewControllers.last!.isKindOfClass(DashBoardVC )) == true)
            {
                let obj = window?.rootViewController?.childViewControllers.last as! DashBoardVC
                let notiVC = obj.childViewControllers.last as! NotificationVC
                notiVC.reloadCell()
            }
        }
        NSNotificationCenter.defaultCenter().postNotificationName(Constants.pushNotification, object: nil)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    //MARK:- UNIQUE Identifier
    static func getUniqueDeviceIdentifierAsString() -> String {
        
        let appName = NSBundle.mainBundle().infoDictionary![kCFBundleNameKey as String] as! String
        
        if let appUUID = SSKeychain.passwordForService(appName, account: "com.neuronimbus.MetroPolis") {
            return appUUID
        }
        else {
            
            let appUUID = UIDevice.currentDevice().identifierForVendor?.UUIDString
            SSKeychain.setPassword(appUUID, forService: appName, account: "com.neuronimbus.MetroPolis")
            return appUUID!
        }
        
        
        
    }
    //MARK: - PushNotificationDelegate Methods
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        print(deviceToken.description)
        deviceTokenString = deviceToken.description.componentsSeparatedByCharactersInSet(NSCharacterSet.alphanumericCharacterSet().invertedSet).joinWithSeparator("")
        
        if deviceTokenString == "" || deviceTokenString.isEmpty == true {
            deviceTokenString = "0"
            NSUserDefaults.standardUserDefaults().setObject(deviceTokenString, forKey: "deviceToken")
        }
        else
        {
        NSUserDefaults.standardUserDefaults().setObject(deviceTokenString, forKey: "deviceToken")
        self.registerDevice()
        }
        
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
        print("DID Fail TO Register FOR Remote Notifications")
        NSUserDefaults.standardUserDefaults().setObject(deviceTokenString, forKey: "Failed")
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("didReceiveRemoteNotification --  \(userInfo)")
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void)
    {
        print("didReceiveRemoteNotification Comp[letion Handler - \(userInfo)")
        savePushMessages(userInfo["aps"] as! [String: AnyObject])
        let aps = userInfo["aps"] as! [String: AnyObject]
        
        let message = aps["alert"]! as! String
        let rateAlert = UIAlertController(title: "Notification", message: message, preferredStyle: .Alert)
        let goToItunesAction = UIAlertAction(title: "Dismiss", style: .Cancel, handler: { (action) -> Void in
            
        })
        
        rateAlert.addAction(goToItunesAction)
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            self.window!.rootViewController?.presentViewController(rateAlert, animated: true, completion: nil)
        })
    }

    
    //MARK:- DEVICE Token
    static func getDeviceToken() -> String {
        var str = NSUserDefaults.standardUserDefaults().stringForKey("deviceToken")
        if str == nil || (str?.isEmpty)! {
            str = "empty"
        }
        
        print("TOKEN FOUND --- \(str?.stringByReplacingOccurrencesOfString(" ", withString: ""))")
        return str!
    }

    
    //MARK:- INDICATOR VIEW
    func createIndicator()
    {
        activityView1 = UIView.init(frame: CGRectMake(0, 0, ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT))
        activityView1.backgroundColor = UIColor.clearColor()
        
        let back = UIImageView.init(frame: CGRectMake(0, 0, ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT))
        back.backgroundColor = UIColor.blackColor()
        back.alpha = 0.5
        activityView1.addSubview(back)
        
        let bgView = UIView.init(frame: CGRectMake((ScreenSize.SCREEN_WIDTH - 120)/2,(ScreenSize.SCREEN_HEIGHT - 150)/2, 120,150))
        bgView.backgroundColor = Colors.appNavigationColor
        bgView.layer.cornerRadius = 18.0
        bgView.layer.shadowColor = Colors.ShadowColor.CGColor
        bgView.clipsToBounds = true
        
        let bgImage = UIImageView.init(frame: CGRectMake(36,36, 48,48))
        bgImage.backgroundColor = UIColor.clearColor()
        bgImage.image = UIImage.init(named: "loader_icon.png")
        
        
        imageForR = UIImageView.init(frame: CGRectMake(25,25, 70,70))
        imageForR.image = UIImage.init(named: "mouse.png")
        imageForR.backgroundColor = UIColor.clearColor()
        
        
        load = UILabel.init(frame: CGRectMake(0, 120, 120, 25))
        load.text = "LOADING..."
        load.textColor = UIColor.whiteColor()
        load.font = UIFont.boldSystemFontOfSize(14.0)
        load.textAlignment = NSTextAlignment.Center
        
        bgView.addSubview(imageForR)
        bgView.addSubview(bgImage)
        bgView.addSubview(load)
        
        activityView1.addSubview(bgView)
        self.window?.addSubview(activityView1)
        self.window?.sendSubviewToBack(activityView1)
    }
    
    func startIndicator()
    {
        
        dispatch_async(dispatch_get_main_queue()) {
            self.window!.addSubview(self.activityView1)
            self.rotate360Degrees()
        }
    }
    
    func stopIndicator()
    {
        dispatch_async(dispatch_get_main_queue()) {
            self.imageForR.layer.removeAllAnimations()
            self.window?.willRemoveSubview(self.activityView1)
            self.activityView1.removeFromSuperview()
        }
    }
    
    func rotate360Degrees(duration: CFTimeInterval = 3.5, completionDelegate: AnyObject? = nil)
    {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI * -2.0)
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = 25
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate
        }
        imageForR.layer.addAnimation(rotateAnimation, forKey: nil)
    }


}

